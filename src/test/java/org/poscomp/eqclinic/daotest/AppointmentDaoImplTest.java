package org.poscomp.eqclinic.daotest;

import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.poscomp.eqclinic.dao.interfaces.AppointmentDao;
import org.poscomp.eqclinic.domain.Appointment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@ContextConfiguration(locations = {"classpath:spring-mvc.xml", "classpath:applicationContext.xml",
                "classpath:applicationContext-hibernate.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional
public class AppointmentDaoImplTest {

    @Autowired
    private AppointmentDao appointmentDao;

    @Test
    public void testSaveApp() {
        Appointment app = new Appointment();
        app.setAptdate(new Date());
        appointmentDao.saveApp(app);
    }

    
    public void testSaveApp(Appointment app){}

    public void testGetAppointmentDoctor(int doctorId){}

    public void testGetAppointmentDoctorStatus(int doctorId, int status){}

    public void testGetAppointmentDoctorAvailable(int doctorId){}

    public void testGetAppointmentDoctorAvailableWD(int doctorId, String aptdate){}

    public void testGetAppointmentDoctorCancelled(int doctorId){}

    public void testGetAppointmentPatient(int patientId){}

    public void testGetAppointmentBySessionId(String sessionId){}

    public void testGetAppointmentByAptId(int aptId){}

    public void testGetAppointmentPatientWD(int patientId, String aptdate){}

    public void testGetStuRequest(int patientId){}

    public void testGetNextAptsSP(int patientId){}

    public void testGetNextAptsStu(int doctorId){}

    public void testGetStuRecentRequest(int doctorId){}

    public void testGetLatestAptByDoctorId(int doctorId){}

    public void testGetLatestAptByPatientId(int patientId){}
    
    
    
}
