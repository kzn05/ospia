package org.poscomp.survey.dao;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.survey.domain.Question;
import org.poscomp.survey.domain.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("surveyDao")
public class SurveyDaoImpl implements SurveyDao {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Survey survey) {
        sessionFactory.getCurrentSession().saveOrUpdate(survey);
    }

    @Override
    public List<Survey> listByUser(int adminId) {
        String hql = "from Survey as s where s.adminid = :adminId";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setInteger("adminId", adminId);
        List<Survey> result = (List<Survey>) query.list();
        return result;
    }

    @Override
    public Survey findById(int id) {
        String hql = "from Survey as s where s.id = :id";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setInteger("id", id);
        Survey result = (Survey) query.uniqueResult();
        return result;
    }

    @Override
    public void update(Survey survey) {
        Session session = sessionFactory.getCurrentSession();
        session.flush();
        session.update(survey);

    }

    @Override
    public List<Survey> findAll(int pageNo, int pageSize) {
        String hql = "from Survey";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        List<Survey> result = (List<Survey>) query.list();
        return result;
    }

    @Override
    public void delete(Survey survey) {
        sessionFactory.getCurrentSession().delete(survey);
    }
}
