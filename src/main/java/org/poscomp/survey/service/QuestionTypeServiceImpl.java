package org.poscomp.survey.service;

import java.util.List;

import org.poscomp.survey.dao.QuestionTypeDao;
import org.poscomp.survey.domain.QuestionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("去uestionTypeService")
public class QuestionTypeServiceImpl implements QuestionTypeService{

    @Autowired
    private QuestionTypeDao questionTypedao; 
    
    public QuestionType findByName(String name){
        return questionTypedao.findByName(name);
    }
    
    public List<QuestionType> findAll(){
        
        return questionTypedao.findAll();
        
    }
    
}
