package org.poscomp.survey.service;

import java.util.List;

import org.poscomp.survey.domain.QuestionType;

public interface QuestionTypeService {

    public QuestionType findByName(String name);
    
    public List<QuestionType> findAll();
    
}
