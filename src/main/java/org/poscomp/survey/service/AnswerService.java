package org.poscomp.survey.service;

import java.util.List;

import org.poscomp.survey.domain.Answer;

public interface AnswerService {

    public void save(Answer answer);
    
    public Answer loadAnswerById(int id);

    public Answer loadAnswerBySessionIdUserId(String sessionId, String userType, int surveyId);
    
    public Answer loadAnswerBySessionIdUserIdUsertype(String sessionId, String userType,int userId, int surveyId);
    
    public List<Answer> loadAnswer();
}
