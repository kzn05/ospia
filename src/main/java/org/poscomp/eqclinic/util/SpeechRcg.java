//package org.poscomp.eqclinic.util;
//
//import java.io.FileInputStream;
//import java.io.IOException;
//
//import org.apache.log4j.Logger;
//
//import edu.cmu.sphinx.api.Configuration;
//import edu.cmu.sphinx.api.SpeechResult;
//import edu.cmu.sphinx.api.StreamSpeechRecognizer;
//
///**
// * Created by Chunfeng Liu on 14/08/15.
// */
//
//public class SpeechRcg {
//
//    private static final Logger logger = Logger.getLogger(SpeechRcg.class);
//
//    public static void main(String[] args) {
//
//        Configuration configuration = new Configuration();
//
//        // Set path to acoustic model.
//        configuration.setAcousticModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us");
//        // Set path to dictionary.
//        configuration.setDictionaryPath("resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict");
//        // Set language model.
//        configuration.setLanguageModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us.lm.dmp");
//
//        StreamSpeechRecognizer recognizer;
//        try {
//            recognizer = new StreamSpeechRecognizer(configuration);
//            recognizer.startRecognition(new FileInputStream("10001-90210-01803.wav"));
//            SpeechResult result;
//            // = recognizer.getResult();
//
//            while ((result = recognizer.getResult()) != null) {
//                System.out.println(result.getHypothesis());
//            }
//
//            // System.out.println(result.getHypothesis());
//            recognizer.stopRecognition();
//
//        } catch (IOException e) {
//            logger.error(e.getMessage(), e);
//        }
//
//    }
//
//}
