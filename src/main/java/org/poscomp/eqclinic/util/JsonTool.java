package org.poscomp.eqclinic.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.poscomp.eqclinic.domain.pojo.RecordingFile;

public class JsonTool {


    public static void main(String[] args) {

        String name = getJsonFileName(
                        "E:/apache-tomcat-7.0.63/webapps/"+ConstantValue.projectName+"/video/video_1440914729409/video_1440914729409");
        System.out.println(name);
    }

    public static String getJsonFileName(String dirName) {

        File directory = null;
        directory = new File(dirName);
        if (directory.isDirectory()) {
            File[] fileArr = directory.listFiles();
            if (fileArr != null) {
                for (int i = 0; i < fileArr.length; i++) {
                    File file = fileArr[i];
                    String[] extName = file.getName().split("\\.");
                    if ("json".equals(extName[1])) {
                        return file.getName();
                    }
                }
            }
        }

        return null;
    }


    public static ArrayList<RecordingFile> loadJsonObject(String path) {

        String JsonContext = readFile(path);
        JSONArray jsonArray = JSONArray.fromObject(JsonContext);

        ArrayList<RecordingFile> rocFiles = new ArrayList<RecordingFile>();

        JSONObject jsonObject = jsonArray.getJSONObject(0);
        String bigFileName = (String) jsonObject.get("name");
        Long startRecordingTime =  (Long) jsonObject.get("createdAt");
        JSONArray files = (JSONArray) jsonObject.get("files");
        

        int largestStartTimeOffset = 0;
        // find largest startTimeOffset
        for (int i = 0; i < files.size(); i++) {
            JSONObject fileObj = files.getJSONObject(i);
            int startTimeOffset = (Integer) fileObj.get("startTimeOffset");
            if (startTimeOffset > largestStartTimeOffset) {
                largestStartTimeOffset = startTimeOffset;
            }
        }

        for (int i = 0; i < files.size(); i++) {
            JSONObject fileObj = files.getJSONObject(i);
            RecordingFile file = new RecordingFile();

            String userType = (String) fileObj.get("connectionData");
            String opentokName = (String) fileObj.get("streamId");
            String fileName = bigFileName + "_" + userType;
            int size = (Integer) fileObj.get("size");
            int startTimeOffset = (Integer) fileObj.get("startTimeOffset");
            int stopTimeOffset = (Integer) fileObj.get("stopTimeOffset");

            file.setFileName(fileName);
            file.setSize(size);
            file.setStartTimeOffset(startTimeOffset);
            file.setStopTimeOffset(stopTimeOffset);
            file.setUserType(userType);
            file.setOpentokName(opentokName);
            file.setTrimmedLength(largestStartTimeOffset - startTimeOffset);
            file.setRecordingTime(startRecordingTime);
            rocFiles.add(file);
        }
        return rocFiles;

    }


    public static String readFile(String Path) {
        BufferedReader reader = null;
        String laststr = "";
        try {
            FileInputStream fileInputStream = new FileInputStream(Path);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
            reader = new BufferedReader(inputStreamReader);
            String tempString = null;
            while ((tempString = reader.readLine()) != null) {
                laststr += tempString;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "[" + laststr + "]";
    }


}
