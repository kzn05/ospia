package org.poscomp.eqclinic.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

import org.apache.log4j.Logger;

import loudness.Loudness;
import pitch.Pitch;
import syllable.Syllable;

import com.mathworks.toolbox.javabuilder.MWArray;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

import endpoint.EndPoint;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class SoundTool {

    private static final Logger logger = Logger.getLogger(SoundTool.class);

    public static void main(String[] args) {

        SoundTool tool = new SoundTool();
        String saveFileName = "video_1472104767272_patient";
        tool.shrinkEndpointFile("C:/videos/" + saveFileName + "_endpoint.txt", "C:/videos/" + saveFileName  + "_endpoint_new.txt", 0.3f);
    }

    // audioFileBaseDirStr, audioFileName
    public void processSound(String soundFileDir, String soundFileName, String saveFileDir, String saveFileName) {

        if (soundFileName.indexOf(".wav") > 0) {
            String[] temp = soundFileName.split("\\.");
            soundFileName = temp[0];
        }

        SoundTool tool = new SoundTool();
        tool.processLoudness(soundFileDir, soundFileName, saveFileDir, saveFileName);
        tool = null;

        // tool = new SoundTool();
        // tool.processPitch(soundFileDir,
        // soundFileName,saveFileDir,saveFileName);
        // tool = null;
        //
        // tool = new SoundTool();
        // tool.processEndpoint(soundFileDir,
        // soundFileName,saveFileDir,saveFileName);
        // tool = null;
        //
        // tool = new SoundTool();
        // tool.shrinkEndpointFile(saveFileDir+"/"+saveFileName+"_endpoint.txt",
        // saveFileDir+"/"+saveFileName+"_endpoint_new.txt",0.3f);
        //
        // // tool = new SoundTool();
        // // tool.processSyllable(soundFileDir,
        // soundFileName,saveFileDir,saveFileName);
        // // tool = null;
        //
        // tool = new SoundTool();
        // tool.processSoundProperty(soundFileDir,
        // soundFileName,saveFileDir,saveFileName);
        // tool = null;
    }

    public void processSyllable(String soundFileDir, String soundFileName, String saveFileDir, String saveFileName) {
        MWNumericArray temp = null;
        Object[] result = null;
        Syllable syllable = null;

        try {
            syllable = new Syllable();
            result = syllable.syllable(1, soundFileDir + "/" + soundFileName + ".wav");

            // the return value is the start time and end time of the syllable
            temp = (MWNumericArray) result[0];
            float[][] syllables = (float[][]) temp.toFloatArray();

            PrintWriter writer = new PrintWriter(saveFileDir + "/" + saveFileName + "_syllable.txt", "UTF-8");
            for (int i = 0; i < syllables.length; i++) {
                float[] entry = syllables[i];
                if (entry.length == 2) {
                    writer.println(entry[0] + "," + entry[1]);
                }
            }
            writer.close();
            syllable.dispose();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            MWArray.disposeArray(temp);
            MWArray.disposeArray(result);
        }
    }

    public void processEndpoint(String soundFileDir, String soundFileName, String saveFileDir, String saveFileName, int fileNumber) {

        MWNumericArray temp = null;
        Object[] result = null;
        EndPoint endp = null;

        try {
            endp = new EndPoint();
            result = endp.endpoint(1, soundFileDir + "/" + soundFileName + ".wav");
            // the return value is the number of window, not the frame number
            temp = (MWNumericArray) result[0];
            float[][] endpoints = (float[][]) temp.toFloatArray();
            PrintWriter writer = new PrintWriter(saveFileDir + "/" + saveFileName + "_endpoint.txt", "UTF-8");
            for (int i = 0; i < endpoints.length; i++) {
                float[] entry = endpoints[i];
                if (entry.length == 2) {
                    writer.println((entry[0]+600*fileNumber) + "," + (entry[1]+600*fileNumber));
                }
            }
            writer.close();
            endp.dispose();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            MWArray.disposeArray(temp);
            MWArray.disposeArray(result);
        }

    }

    public void shrinkEndpointFile(String inputFile, String outputFile, float interval) {
        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            br = new BufferedReader(new FileReader(inputFile));
            bw = new BufferedWriter(new FileWriter(outputFile));

            String line = br.readLine();

            float prevStart = 0;
            float prevEnd = 0;

            while (line != null) {
                line = line.trim();
                String[] values = line.split(",");

                float start = Float.parseFloat(values[0]);
                float end = Float.parseFloat(values[1]);

                if (prevStart == 0 && prevEnd == 0) {
                    prevStart = start;
                    prevEnd = end;
                } else {
                    // have overlapping
                    if (start - prevEnd < interval || start < prevEnd) {
                        prevEnd = end;
                    } else {
                        bw.write(prevStart + "," + prevEnd);
                        bw.newLine();
                        prevStart = start;
                        prevEnd = end;
                    }
                }

                line = br.readLine();
            }

            br.close();
            bw.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void processPitch(String soundFileDir, String soundFileName, String saveFileDir, String saveFileName) {

        MWNumericArray temp = null;
        Object[] result = null;
        Pitch pitch = null;

        try {
            pitch = new Pitch();
            result = pitch.pitch(2, soundFileDir + "/" + soundFileName + ".wav");

            temp = (MWNumericArray) result[0];
            float[][] pitches = (float[][]) temp.toFloatArray();
            String totalPitch = "";
            for (int i = 0; i < pitches[0].length; i++) {
                float pitchValue = 0;
                if (Float.isNaN(pitches[0][i])) {
                    pitchValue = 0;
                } else {
                    pitchValue = pitches[0][i];
                }

                if (i != 0) {
                    totalPitch += "," + pitchValue;
                } else {
                    totalPitch += pitchValue;
                }
            }

            PrintWriter writer = new PrintWriter(saveFileDir + "/" + saveFileName + "_pitch.txt", "UTF-8");
            writer.println(totalPitch);
            writer.close();
            pitch.dispose();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            MWArray.disposeArray(temp);
            MWArray.disposeArray(result);
        }

    }

    public void processLoudness(String soundFileDir, String soundFileName, String saveFileDir, String saveFileName) {
        // get the power of sound
        MWNumericArray temploudness = null;
        MWNumericArray temptime = null;
        Object[] result = null;
        Loudness loud = null;

        try {
            loud = new Loudness();

            result = loud.loudness(2, soundFileDir + "/" + soundFileName + ".wav");
            temploudness = (MWNumericArray) result[0];
            temptime = (MWNumericArray) result[1];

            // get the total length of speech
            float[][] time = (float[][]) temptime.toFloatArray();
            float totalLength = time[0][0];

            // get the power of each speech window
            float[][] loudness = (float[][]) temploudness.toFloatArray();
            String totalLoudness = "";
            for (int i = 0; i < loudness[0].length; i++) {
                if (i != 0) {
                    totalLoudness += "," + loudness[0][i];
                } else {
                    totalLoudness += loudness[0][i];
                }
            }
            PrintWriter writer = new PrintWriter(saveFileDir + "/" + saveFileName + "_loudness.txt", "UTF-8");
            // writer.println(totalLength);
            writer.println(totalLoudness);
            writer.close();
            loud.dispose();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            MWArray.disposeArray(temptime);
            MWArray.disposeArray(temploudness);
            MWArray.disposeArray(result);
        }

    }
    
    public void processLoudness(String soundFileDir, String soundFileName, String saveFileDir, String saveFileName, float totalLength) {
        // get the power of sound
        MWNumericArray temploudness = null;
        MWNumericArray temptime = null;
        Object[] result = null;
        Loudness loud = null;

        try {
            loud = new Loudness();

            result = loud.loudness(2, soundFileDir + "/" + soundFileName + ".wav");
            temploudness = (MWNumericArray) result[0];
            temptime = (MWNumericArray) result[1];

            // get the total length of speech
            float[][] time = (float[][]) temptime.toFloatArray();

            // get the power of each speech window
            float[][] loudness = (float[][]) temploudness.toFloatArray();
            String totalLoudness = "";
            for (int i = 0; i < loudness[0].length; i++) {
                if (i != 0) {
                    totalLoudness += "," + loudness[0][i];
                } else {
                    totalLoudness += loudness[0][i];
                }
            }
            PrintWriter writer = new PrintWriter(saveFileDir + "/" + saveFileName + "_loudness.txt", "UTF-8");
            writer.println(totalLength);
            writer.println(totalLoudness);
            writer.close();
            loud.dispose();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            MWArray.disposeArray(temptime);
            MWArray.disposeArray(temploudness);
            MWArray.disposeArray(result);
        }

    }
    

    public float processSoundProperty(String soundFileDir, String soundFileName, String saveFileDir,
                    String saveFileName) {
        // get the power of sound
        MWNumericArray temptime = null;
        MWNumericArray tempFrameNo = null;
        Object[] result = null;
        Loudness loud = null;

        try {
            loud = new Loudness();

            result = loud.waveproperty(2, soundFileDir + "/" + soundFileName + ".wav");
            temptime = (MWNumericArray) result[0];
            tempFrameNo = (MWNumericArray) result[1];

            // get the total length of speech
            float[][] time = (float[][]) temptime.toFloatArray();
            float totalLength = time[0][0];

            // get the total length of speech
            float[][] frame = (float[][]) tempFrameNo.toFloatArray();
            int totalFrame = (int) frame[0][0];

            PrintWriter writer = new PrintWriter(saveFileDir + "/" + saveFileName + "_waveproperty.txt", "UTF-8");
            writer.println(totalLength);
            writer.println(totalFrame);
            writer.close();
            loud.dispose();
            
            return totalLength;

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            MWArray.disposeArray(tempFrameNo);
            MWArray.disposeArray(temptime);
            MWArray.disposeArray(result);
        }

        return 0;
    }

}
