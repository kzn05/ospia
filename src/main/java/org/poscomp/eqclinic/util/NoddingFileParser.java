package org.poscomp.eqclinic.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public class NoddingFileParser {

    private static final Logger logger = Logger.getLogger(NoddingFileParser.class);
    
    public static List<Integer> readFile2(String fileName) {

        List<Integer> yPoints = new ArrayList<Integer>();
        int lineNo = 0;
        try {
            FileReader reader = new FileReader(new File(fileName));
            BufferedReader br = new BufferedReader(reader);

            String line = br.readLine();

            while (line != null && !"".equals(line)) {
                lineNo++;
                line = br.readLine();
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return yPoints;
    }

    public static List<Integer> readFile(String fileName) {

        List<Integer> yPoints = new ArrayList<Integer>();

        try {
            FileReader reader = new FileReader(new File(fileName));
            BufferedReader br = new BufferedReader(reader);

            String line = br.readLine();
            while (line != null && !"".equals(line)) {
                int ypoint = Integer.parseInt(line.trim());
                yPoints.add(ypoint);
                line = br.readLine();
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return yPoints;
    }

    public static List<Float> readFileX(String fileName) {

        List<Float> xPoints = new ArrayList<Float>();

        try {
            FileReader reader = new FileReader(new File(fileName));
            BufferedReader br = new BufferedReader(reader);

            String line = br.readLine();
            while (line != null && !"".equals(line)) {
                float xpoint = Float.parseFloat(line.trim().split(",")[0]);
                xPoints.add(xpoint);
                line = br.readLine();
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return xPoints;
    }

    public static List<Float> readFileY(String fileName) {

        List<Float> yPoints = new ArrayList<Float>();

        try {
            FileReader reader = new FileReader(new File(fileName));
            BufferedReader br = new BufferedReader(reader);

            String line = br.readLine();
            while (line != null && !"".equals(line)) {
                float xpoint = Float.parseFloat(line.trim().split(",")[1]);
                yPoints.add(xpoint);
                line = br.readLine();
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return yPoints;
    }

    public static List<Integer> readFileColumn(String fileName, int colNo) {

        List<Integer> yPoints = new ArrayList<Integer>();

        try {
            FileReader reader = new FileReader(new File(fileName));
            BufferedReader br = new BufferedReader(reader);

            String line = br.readLine();
            while (line != null && !"".equals(line)) {
                int xpoint = Integer.parseInt(line.trim().split(",")[colNo]);
                yPoints.add(xpoint);
                line = br.readLine();
            }

            br.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return yPoints;
    }

}
