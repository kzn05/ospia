package org.poscomp.eqclinic.controller;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;
import org.poscomp.eqclinic.domain.CalibrationUser;
import org.poscomp.eqclinic.domain.pojo.ReturnValue;
import org.poscomp.eqclinic.service.interfaces.CalibrationUserService;
import org.poscomp.eqclinic.util.ReliabilityCalculator;
import org.poscomp.survey.domain.Answer;
import org.poscomp.survey.domain.Survey;
import org.poscomp.survey.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Controller
public class CalibrationUserController {

    private static final Logger logger = Logger.getLogger(CalibrationUserController.class);

    @Autowired
    private CalibrationUserService calibrationUserService;
    
    @Autowired
    private SurveyService surveyService;

    @RequestMapping(value = "/cali/savecalibrationuser", method = {RequestMethod.POST},
                    headers = {"Content-type=application/json"})
    @ResponseBody
    public ReturnValue saveCalibrationUser(HttpServletRequest request, @RequestBody CalibrationUser calibrationUser) {
        System.out.println(calibrationUser);

        ReturnValue rv = new ReturnValue();

        if (calibrationUser != null) {
            calibrationUser.setCrateAt(System.currentTimeMillis());
            calibrationUserService.saveCalibrationUser(calibrationUser);
            request.getSession().setAttribute("calibrationUser", calibrationUser);
            rv.setCode(1);

        } else {
            rv.setCode(0);

        }

        return rv;
    }

    @RequestMapping(value = "/cali/soca_cali/{strValue}", method = {RequestMethod.GET})
    public String redirect_soca_tut(HttpServletRequest request,@PathVariable String strValue) {
        
        
        String[] values= strValue.split(",");
        
        if(values==null || values.length==0 || values[0]==null || values[0].equals("")){
            return "redirect:/cali/calibrate_info";
        }
        
        int userId = Integer.parseInt(values[0]);
        int sessionId = Integer.parseInt(values[1]);
        
        int goldStandardId = 0;

        if (sessionId == 1) {
            goldStandardId = 1;
        } else if (sessionId == 2) {
            goldStandardId = 2;
        } else if (sessionId == 3) {
            goldStandardId = 3;
        }

        CalibrationSurveyAnswer goldAnswer = calibrationUserService.getCalibrationSurveyAnswerById(goldStandardId);
        CalibrationSurveyAnswer testAnswer =
                        calibrationUserService.getCalibrationSurveyAnswerByUserIdSessionId(userId, sessionId);
        
        if(testAnswer==null){
            request.setAttribute("surveyId", 1);
            request.setAttribute("ispreview", 0);
            request.setAttribute("sessionId", sessionId);
            return "cali/calibrate_soca";
        }else{
            
            request.setAttribute("answerId", testAnswer.getId());
            request.setAttribute("surveyId", 1);
            request.setAttribute("ispreview", 0);
            request.setAttribute("sessionId", sessionId);
            return "cali/calibrate_soca";
        }
    }

    @RequestMapping(value = "/cali/soca_cali2/{sessionId}", method = {RequestMethod.GET})
    public String redirect_soca_tut2(HttpServletRequest request, @PathVariable int sessionId) {
        
        request.setAttribute("surveyId", 1);
        request.setAttribute("ispreview", 0);
        request.setAttribute("sessionId", sessionId);
        return "cali/calibrate_soca";
    }
    

    @RequestMapping(value = "/cali/saveanswer", method = {RequestMethod.POST},
                    headers = {"Content-type=application/json"})
    @ResponseBody
    public ReturnValue saveAnswer(HttpServletRequest request, @RequestBody Answer answer) {

        CalibrationUser calibrationUser = (CalibrationUser) request.getSession().getAttribute("calibrationUser");
        CalibrationSurveyAnswer cAnswer = new CalibrationSurveyAnswer();
        cAnswer.setCreateAt(System.currentTimeMillis());
        cAnswer.setCalibrationUserId(calibrationUser.getId());
        cAnswer.setSurveyAnswer(answer.getContent());
        cAnswer.setVideoId(Integer.parseInt(answer.getSessionId()));

        calibrationUserService.saveCalibrationSurveyAnswer(cAnswer);

        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        return rv;
    }
    
    
    @RequestMapping(value = "/cali/loadanswercali/{answerId}", method = { RequestMethod.GET })
    public void loadAnswer(HttpServletRequest request, PrintWriter printWriter, @PathVariable int answerId) {
        CalibrationSurveyAnswer answerCali = calibrationUserService.getCalibrationSurveyAnswerById(answerId);
        Survey survey = surveyService.findById(1);
        Answer answer = new Answer();
        answer.setSurvey(survey);
        answer.setContent(answerCali.getSurveyAnswer());
        JsonConfig config = new JsonConfig();
        config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        JSONObject jsonArray = JSONObject.fromObject(answer, config);
        printWriter.write(jsonArray.toString());
        printWriter.flush();
        printWriter.close();

    }
    
    

    @RequestMapping(value = "/cali/reliability/{userId}/{sessionId}", method = {RequestMethod.GET})
    @ResponseBody
    public ReturnValue calculateReliability(HttpServletRequest request, @PathVariable int userId, @PathVariable int sessionId) {

        int goldStandardId = 0;

        if (sessionId == 1) {
            goldStandardId = 1;
        } else if (sessionId == 2) {
            goldStandardId = 2;
        } else if (sessionId == 3) {
            goldStandardId = 3;
        }

        CalibrationSurveyAnswer goldAnswer = calibrationUserService.getCalibrationSurveyAnswerById(goldStandardId);
        CalibrationSurveyAnswer testAnswer =
                        calibrationUserService.getCalibrationSurveyAnswerByUserIdSessionId(userId, sessionId);

        if(testAnswer == null){
            ReturnValue rv = new ReturnValue();
            rv.setCode(0);
            rv.setContent("Sorry, you have not assess the this consultation. Please assess it first, and then check your reliability.");
            return rv;
        }
        
        double result = ReliabilityCalculator.calculate(goldAnswer, testAnswer);
        
        ReturnValue rv = new ReturnValue();
        rv.setCode(1);
        rv.setContent("By comparing with our gold standar, the reliability of  your assessment is: "+String.format("%.3f", result)+" (the higher the score is, the more reliable your assessment.) ");
        return rv;
        
    }



}
