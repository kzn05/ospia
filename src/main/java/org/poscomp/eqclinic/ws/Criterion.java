/**
 * Criterion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.poscomp.eqclinic.ws;

public class Criterion  implements java.io.Serializable {
    private java.lang.String criterionNum;

    private org.poscomp.eqclinic.ws.TemplateField title;

    private org.poscomp.eqclinic.ws.TemplateField description;

    private org.poscomp.eqclinic.ws.CriterionGrade[] criterionGradeList;

    private java.lang.String defaultGrade;

    public Criterion() {
    }

    public Criterion(
           java.lang.String criterionNum,
           org.poscomp.eqclinic.ws.TemplateField title,
           org.poscomp.eqclinic.ws.TemplateField description,
           org.poscomp.eqclinic.ws.CriterionGrade[] criterionGradeList,
           java.lang.String defaultGrade) {
           this.criterionNum = criterionNum;
           this.title = title;
           this.description = description;
           this.criterionGradeList = criterionGradeList;
           this.defaultGrade = defaultGrade;
    }


    /**
     * Gets the criterionNum value for this Criterion.
     * 
     * @return criterionNum
     */
    public java.lang.String getCriterionNum() {
        return criterionNum;
    }


    /**
     * Sets the criterionNum value for this Criterion.
     * 
     * @param criterionNum
     */
    public void setCriterionNum(java.lang.String criterionNum) {
        this.criterionNum = criterionNum;
    }


    /**
     * Gets the title value for this Criterion.
     * 
     * @return title
     */
    public org.poscomp.eqclinic.ws.TemplateField getTitle() {
        return title;
    }


    /**
     * Sets the title value for this Criterion.
     * 
     * @param title
     */
    public void setTitle(org.poscomp.eqclinic.ws.TemplateField title) {
        this.title = title;
    }


    /**
     * Gets the description value for this Criterion.
     * 
     * @return description
     */
    public org.poscomp.eqclinic.ws.TemplateField getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Criterion.
     * 
     * @param description
     */
    public void setDescription(org.poscomp.eqclinic.ws.TemplateField description) {
        this.description = description;
    }


    /**
     * Gets the criterionGradeList value for this Criterion.
     * 
     * @return criterionGradeList
     */
    public org.poscomp.eqclinic.ws.CriterionGrade[] getCriterionGradeList() {
        return criterionGradeList;
    }


    /**
     * Sets the criterionGradeList value for this Criterion.
     * 
     * @param criterionGradeList
     */
    public void setCriterionGradeList(org.poscomp.eqclinic.ws.CriterionGrade[] criterionGradeList) {
        this.criterionGradeList = criterionGradeList;
    }

    public org.poscomp.eqclinic.ws.CriterionGrade getCriterionGradeList(int i) {
        return this.criterionGradeList[i];
    }

    public void setCriterionGradeList(int i, org.poscomp.eqclinic.ws.CriterionGrade _value) {
        this.criterionGradeList[i] = _value;
    }


    /**
     * Gets the defaultGrade value for this Criterion.
     * 
     * @return defaultGrade
     */
    public java.lang.String getDefaultGrade() {
        return defaultGrade;
    }


    /**
     * Sets the defaultGrade value for this Criterion.
     * 
     * @param defaultGrade
     */
    public void setDefaultGrade(java.lang.String defaultGrade) {
        this.defaultGrade = defaultGrade;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Criterion)) return false;
        Criterion other = (Criterion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.criterionNum==null && other.getCriterionNum()==null) || 
             (this.criterionNum!=null &&
              this.criterionNum.equals(other.getCriterionNum()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.criterionGradeList==null && other.getCriterionGradeList()==null) || 
             (this.criterionGradeList!=null &&
              java.util.Arrays.equals(this.criterionGradeList, other.getCriterionGradeList()))) &&
            ((this.defaultGrade==null && other.getDefaultGrade()==null) || 
             (this.defaultGrade!=null &&
              this.defaultGrade.equals(other.getDefaultGrade())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCriterionNum() != null) {
            _hashCode += getCriterionNum().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getCriterionGradeList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCriterionGradeList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCriterionGradeList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDefaultGrade() != null) {
            _hashCode += getDefaultGrade().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Criterion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "Criterion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criterionNum");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CriterionNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Title"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "TemplateField"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criterionGradeList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CriterionGradeList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:DefaultNamespace", "CriterionGrade"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("defaultGrade");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DefaultGrade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
