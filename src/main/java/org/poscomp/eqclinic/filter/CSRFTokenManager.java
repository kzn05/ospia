package org.poscomp.eqclinic.filter;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public final class CSRFTokenManager {
    static final String CSRF_PARAM_NAME = "CSRFToken";
    public static final String CSRF_TOKEN_FOR_SESSION_ATTR_NAME = CSRFTokenManager.class.getName() + ".tokenval";

    public static String getTokenForSession(HttpSession session) {
        String token = null;
        synchronized (session) {
            token = (String) session
            .getAttribute(CSRF_TOKEN_FOR_SESSION_ATTR_NAME);
            if (null == token) {
                token = UUID.randomUUID().toString();
                session.setAttribute(CSRF_TOKEN_FOR_SESSION_ATTR_NAME, token);
            }
        }
        return token;
    }

    public static String getTokenFromRequest(HttpServletRequest request) {
        return request.getParameter(CSRF_PARAM_NAME);
    }

    private CSRFTokenManager() {

    };

    public static boolean isValidCsrfHeaderToken(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (request.getHeader("__RequestVerificationToken") == null
                        || session.getAttribute(CSRFTokenManager.CSRF_TOKEN_FOR_SESSION_ATTR_NAME) == null
                        || !request.getHeader("__RequestVerificationToken").equals(
                                        session.getAttribute(CSRFTokenManager.CSRF_TOKEN_FOR_SESSION_ATTR_NAME)
                                                        .toString())) {
            return false;
        }
        return true;
    }

}
