package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "loginlog", catalog = "ospia")
public class LoginLog implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer logId;

    private Integer doctorId;

    private Long loginAt;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "logId", unique = true, nullable = false)
    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    @Column(name = "doctorId")
    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    @Column(name = "loginAt")
    public Long getLoginAt() {
        return loginAt;
    }

    public void setLoginAt(Long loginAt) {
        this.loginAt = loginAt;
    }


}
