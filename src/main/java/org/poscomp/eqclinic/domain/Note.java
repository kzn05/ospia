package org.poscomp.eqclinic.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Entity
@Table(name = "Note", catalog = "ospia")
public class Note implements Comparable<Note>, Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String sessionId;
    // 1: tutor  2: patient
    private Integer noterType;
    private Integer noterId;
    private Double time;
    private String noteContent;
    private Long realTime;
    private Long startTime;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "sessionId", length=300)
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Column(name = "noterType")
    public Integer getNoterType() {
        return noterType;
    }

    public void setNoterType(Integer noterType) {
        this.noterType = noterType;
    }

    @Column(name = "noterId")
    public Integer getNoterId() {
        return noterId;
    }

    public void setNoterId(Integer noterId) {
        this.noterId = noterId;
    }

    @Column(name = "time")
    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    @Column(name = "noteContent", length=10000)
    public String getNoteContent() {
        return noteContent;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }

    @Column(name = "realTime")
    public Long getRealTime() {
        return realTime;
    }

    public void setRealTime(Long realTime) {
        this.realTime = realTime;
    }

    @Column(name = "startTime")
    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    @Override
    public int compareTo(Note note) {

        double time = note.getTime();

        if (this.getTime() - time > 0) {
            return 1;
        } else if (this.getTime() - time < 0) {
            return -1;
        } else {
            return 0;
        }
    }

}
