package org.poscomp.eqclinic.domain.pojo;

public class NoddingObject {

    public String type;
    public float startTime;
    public float endTime;
    public float moveValue;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getStartTime() {
        return startTime;
    }

    public void setStartTime(float startTime) {
        this.startTime = startTime;
    }

    public float getEndTime() {
        return endTime;
    }

    public void setEndTime(float endTime) {
        this.endTime = endTime;
    }

    public float getMoveValue() {
        return moveValue;
    }

    public void setMoveValue(float moveValue) {
        this.moveValue = moveValue;
    }

}