package org.poscomp.eqclinic.domain.pojo;

import java.util.Collection;

import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.Teacher;
import org.poscomp.survey.domain.Admin;
import org.springframework.security.core.GrantedAuthority;

public class MyUserPrincipal extends org.springframework.security.core.userdetails.User {

    private Patient patient;
    private Doctor doctor;
    private Teacher teacher;
    private Admin admin;

    public MyUserPrincipal(String username, String password, Collection<? extends GrantedAuthority> authorities,
                    Patient patient, Doctor doctor, Teacher teacher, Admin admin) {

        super(username, password, authorities);

        this.patient = patient;
        this.doctor = doctor;
        this.teacher = teacher;
        this.admin = admin;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }


}
