package org.poscomp.eqclinic.domain.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
public class SpeakingPeriod implements Comparable<SpeakingPeriod>, Serializable {

    private static final long serialVersionUID = 1L;

    private float endPointStart;
    private float endPointEnd;
    private String subject;
    private float period;

    public float getEndPointStart() {
        return endPointStart;
    }

    public void setEndPointStart(float endPointStart) {
        this.endPointStart = endPointStart;
    }

    public float getEndPointEnd() {
        return endPointEnd;
    }

    public void setEndPointEnd(float endPointEnd) {
        this.endPointEnd = endPointEnd;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public ArrayList<SpeakingPeriod> copy(ArrayList<SpeakingPeriod> sps) {
        ArrayList<SpeakingPeriod> result = new ArrayList<SpeakingPeriod>();
        for (SpeakingPeriod sp : sps) {
            SpeakingPeriod tempSp = new SpeakingPeriod();
            tempSp.setEndPointEnd(sp.getEndPointEnd());
            tempSp.setEndPointStart(sp.getEndPointStart());
            tempSp.setSubject(sp.getSubject());
            tempSp.setPeriod(sp.getPeriod());
            result.add(tempSp);
        }
        return result;
    }

    @Override
    public int compareTo(SpeakingPeriod sp) {
        if (this.endPointStart - sp.getEndPointStart() > 0) {
            return 1;
        } else if (this.endPointStart - sp.getEndPointStart() < 0) {
            return -1;
        } else {
            return 0;
        }
    }
}
