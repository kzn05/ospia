package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.ApplicationDao;
import org.poscomp.eqclinic.dao.interfaces.AvailableStudentGroupDao;
import org.poscomp.eqclinic.domain.Application;
import org.poscomp.eqclinic.domain.AvailableStudentGroup;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "availableStudentGroupDao")
public class AvailableStudentGroupDaoImpl implements AvailableStudentGroupDao {

    private static final Logger logger = Logger.getLogger(AvailableStudentGroupDaoImpl.class);

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public String getAllAvailableGroups() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from AvailableStudentGroup as a where a.status=1";
        Query query = session.createQuery(hql);
        List<AvailableStudentGroup> list = (ArrayList<AvailableStudentGroup>) query.list();

        String result = "";

        for (int i = 0; i < list.size(); i++) {

            AvailableStudentGroup group = list.get(i);

            if (i == 0) {
                result = group.getGroupId() + "";
            } else {
                result += "," + group.getGroupId();
            }

        }


        return result;
    }

}
