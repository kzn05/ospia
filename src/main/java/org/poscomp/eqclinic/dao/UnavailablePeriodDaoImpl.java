package org.poscomp.eqclinic.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.UnavailablePeriodDao;
import org.poscomp.eqclinic.domain.UnavailablePeriod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Repository(value = "unavailablePeriodDao")
public class UnavailablePeriodDaoImpl implements UnavailablePeriodDao {

    private static final Logger logger = Logger.getLogger(UnavailablePeriodDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveUnavailablePeriod(UnavailablePeriod period) {
        sessionFactory.getCurrentSession().saveOrUpdate(period);
        logger.info("you have insert an unavailable period");
    }

    @Override
    public UnavailablePeriod getUnavailablePeriodById(int id) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from UnavailablePeriod as p where p.id=:id";
        Query query = session.createQuery(hql);
        query.setInteger("id", id);
        UnavailablePeriod result = (UnavailablePeriod) query.uniqueResult();
        return result;
    }

    @Override
    public List<UnavailablePeriod> getUnavailablePeriod() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from UnavailablePeriod as p where p.status=1";
        Query query = session.createQuery(hql);
        List<UnavailablePeriod> result = (List<UnavailablePeriod>) query.list();
        return result;
    }
    
}
