package org.poscomp.eqclinic.dao.interfaces;

import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;
import org.poscomp.eqclinic.domain.CalibrationUser;


/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface CalibrationUserDao {
    
    public void saveCalibrationUser(CalibrationUser cUser);
    public void saveCalibrationSurveyAnswer(CalibrationSurveyAnswer answer);
    public CalibrationSurveyAnswer getCalibrationSurveyAnswerById(int id);
    public CalibrationSurveyAnswer getCalibrationSurveyAnswerByUserIdSessionId(int userId, int sessionId);
    
}
