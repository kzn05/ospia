package org.poscomp.eqclinic.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.poscomp.eqclinic.dao.interfaces.StudentGroupDao;
import org.poscomp.eqclinic.dao.interfaces.TeacherDao;
import org.poscomp.eqclinic.domain.StudentGroup;
import org.poscomp.eqclinic.domain.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */
@Repository(value = "studentGroupDao")
public class StudentGroupDaoImpl implements StudentGroupDao {

    private static final Logger logger = Logger.getLogger(StudentGroupDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public StudentGroup getStudentGroupByStudentId(String studentId) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from StudentGroup as t where t.studentId=:studentId";
        Query query = session.createQuery(hql);
        query.setString("studentId", studentId);
        StudentGroup result = (StudentGroup) query.uniqueResult();
        return result;
    }

	@Override
	public List<StudentGroup> getAllUnParticipatedStudent() {
		//select studentId from studentgroup where groupNumber=3 and studentId not in ( select doctor.userId from video, doctor where video.videoType=1 and video.doctorId=doctor.doctorId );
		
		Session session = sessionFactory.getCurrentSession();
        String hql = "select sg from StudentGroup as sg where sg.groupNumber=3 and sg.studentId not in (select d.userId from Video as v, Doctor as d where v.videoType=1 and v.doctorId=d.doctorId)";
        Query query = session.createQuery(hql);
        List<StudentGroup> result = (List<StudentGroup>)query.list();
        return result;
	}

}
