package org.poscomp.eqclinic.service;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.CalibrationUserDao;
import org.poscomp.eqclinic.domain.CalibrationSurveyAnswer;
import org.poscomp.eqclinic.domain.CalibrationUser;
import org.poscomp.eqclinic.service.interfaces.CalibrationUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "calibrationUserService")
public class CalibrationUserServiceImpl implements CalibrationUserService {

    private static final Logger logger = Logger.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private CalibrationUserDao calibrationUserDao;

    @Override
    public void saveCalibrationUser(CalibrationUser cUser) {
        calibrationUserDao.saveCalibrationUser(cUser);
    }

    @Override
    public void saveCalibrationSurveyAnswer(CalibrationSurveyAnswer answer) {
        calibrationUserDao.saveCalibrationSurveyAnswer(answer);
    }

    @Override
    public CalibrationSurveyAnswer getCalibrationSurveyAnswerById(int id) {
        return calibrationUserDao.getCalibrationSurveyAnswerById(id);
    }

    @Override
    public CalibrationSurveyAnswer getCalibrationSurveyAnswerByUserIdSessionId(int userId, int sessionId) {
        return calibrationUserDao.getCalibrationSurveyAnswerByUserIdSessionId(userId, sessionId);
    }

}
