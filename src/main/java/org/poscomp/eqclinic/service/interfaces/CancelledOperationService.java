package org.poscomp.eqclinic.service.interfaces;

import org.poscomp.eqclinic.domain.CancelledOperation;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface CancelledOperationService {

    public void saveCancelledOperation(CancelledOperation operation);
}
