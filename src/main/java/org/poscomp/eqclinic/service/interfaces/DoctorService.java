package org.poscomp.eqclinic.service.interfaces;

import java.util.List;

import org.poscomp.eqclinic.domain.Doctor;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

public interface DoctorService {
    public Doctor doctorLogin(String username, String password);

    public Doctor getDoctorByDoctorId(int doctorId);

    public Doctor getDoctorByUsername(String username);

    public List<Doctor> getAllDoctors();

    public void saveDoctor(Doctor doctor);

}
