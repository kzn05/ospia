package org.poscomp.eqclinic.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.NoteDao;
import org.poscomp.eqclinic.domain.Appointment;
import org.poscomp.eqclinic.domain.Note;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.service.interfaces.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service(value = "noteService")
public class NoteServiceImpl implements NoteService {

    private static final Logger logger = Logger.getLogger(NoteServiceImpl.class);

    @Autowired
    private NoteDao noteDao;

    @Override
    public void saveNote(Note note) {
        noteDao.saveNote(note);
    }

    @Override
    public List<Note> getNoteBySessionId(String sessionId) {
        return noteDao.getNoteBySessionId(sessionId);
    }

    @Override
    public List<Note> getNoteBySessionIdNoter(String sessionId, int noterType, int noterId) {
        return noteDao.getNoteBySessionIdNoter(sessionId, noterType, noterId);
    }

    @Override
    public int saveNotePatient(Note note, Appointment apt, Patient patient) {
        long recordStartTime = apt.getStartRecTime();

        if (note != null) {
            note.setNoterId(patient.getPatientid());
            note.setNoterType(2);   
            note.setStartTime(recordStartTime);
            note.setTime((note.getRealTime() - recordStartTime) / 1000.0);
            this.saveNote(note);
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int saveNoteTutor(Note note) {
        if (note != null) {
            this.saveNote(note);
            return 1;
        } else {
            return 0;
        }

    }

    @Override
    public void deleteNote(Note note) {
        noteDao.deleteNote(note);
    }

    @Override
    public Note getNoteById(int noteId) {
        return noteDao.getNoteById(noteId);
    }

    @Override
    public List<Note> getNoteBySessionIdType(String sessionId, int noterType, int noteType) {
        return noteDao.getNoteBySessionIdType(sessionId, noterType, noteType);
    }

    @Override
    public List<Note> getNoteBySessionIdTypeAll(String sessionId, int noterType) {
        return noteDao.getNoteBySessionIdTypeAll(sessionId, noterType);
    }
}
