package org.poscomp.eqclinic.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.poscomp.eqclinic.dao.interfaces.DoctorDao;
import org.poscomp.eqclinic.domain.Doctor;
import org.poscomp.eqclinic.domain.Patient;
import org.poscomp.eqclinic.domain.pojo.MyUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Chunfeng Liu on 14/08/15.
 */

@Service
public class DoctorUserDetailServiceImpl implements UserDetailsService {

    private static final Logger logger = Logger.getLogger(DoctorUserDetailServiceImpl.class);

    @Autowired
    private DoctorDao doctorDao;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        MyUserPrincipal user = null;  
        
        try {  
            Doctor doctor = doctorDao.getDoctorByUsername(username);
            if(doctor==null){
                return null;
            }
            List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);  
            authList.add(new GrantedAuthorityImpl("ROLE_DOCTOR")); 
            user = new MyUserPrincipal(doctor.getUsername(), doctor.getPassword().toLowerCase(),authList, null, doctor, null, null);  
        } catch (Exception e) {  
            e.printStackTrace();
            logger.error("Error in retrieving user");  
            throw new UsernameNotFoundException("Error in retrieving user");  
        }  
  
        return user;  
        
    }
    
}
