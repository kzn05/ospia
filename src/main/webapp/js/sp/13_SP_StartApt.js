$(document).ready(function(){
	
	connectionCount = 0;
	var cameraWorking = false;
	
	if (OT.checkSystemRequirements() == 0) {
		OT.log("The client does not support WebRTC.");
	}
	 
	// --------------------- this is the session of original session: start-------------------------------
	session = OT.initSession(patientApiKeyBaseline, patientSessionIdBaseline);

	session.on({
		connectionCreated: function (event) {
			connectionCount++;
			OT.log(connectionCount + " connections.");
			
			console.log(connectionCount + " connections.");
		},
		
		connectionDestroyed: function (event) {
			connectionCount--;
			OT.log(connectionCount + " connections.");
			
			console.log(connectionCount + " connections.");
		},
		
		sessionDisconnected: function sessionDisconnectHandler(event) {
	    // The event is defined by the SessionDisconnectEvent class
			if (event.reason == "networkDisconnected") {
				alert("Your network connection terminated.");
			}
		},

		streamCreated: function(event) {
			OT.log("New stream in the session: " + event.stream.streamId);
			var subscriberProperties = {resolution: "1280x720", insertMode: "append", width:640, height:480};
//			var subscriberProperties = {resolution: "1280x720",insertMode: "append"};
			session.subscribe(event.stream, 'baselineContainer', subscriberProperties, function (error) {
				if (error) {
					alert(error);
				} else {
					$('#oppositeContainerText').remove();
					OT.log("Subscriber added for stream: " + event.stream.streamId);
				}
			});
		},
	});

	session.connect(patientTokenBaseline, function(error) {
		if (error) {
			alert("Unable to connect: "+error.code+", "+error.message);
			return;
		}
		
		OT.log("Connected to the session.");
		

		if (session.capabilities.publish != 1) {
			alert("You cannot publish an audio-video stream.");
			return;
		}
			
	    // Replace with the replacement element ID:
		 var publisherProperties = {resolution: "1280x720", width:640, height:480};
		 //   var publisherProperties = {resolution: "1280x720"};
//		var publisherProperties = {};
		publisher = OT.initPublisher('baselineContainer', publisherProperties, function(error){
			if (error) {
				OT.log("Unable to publish stream: ", error.message);
				return;
			}
			
			publisher.on({
			    streamCreated: function (event) {
			    		OT.log("Publisher started streaming.");
			    		console.log("Publisher started streaming.");
			    		cameraWorking = true;
			    		$("#startBaseline").removeAttr("disabled");
			    },
			    streamDestroyed: function (event) {
			    		OT.log("Publisher stopped streaming. Reason: "
			               + event.reason);
			    	}
		    });
			
	        if (publisher) {
	        		session.publish(publisher);
	        }
		});
	});
	
	
	
	
	$("#startBaseline").click(function(){
		
		if(cameraWorking == false){
			alert("Sorry, you cannot start it now. Please refresh this page, and accept requests to share your camera and microphone!");
			return;
		}
		
		if($("#startBaseline").val()=="Start"){
			$("#startBaseline").val("Stop");
			$.ajax({
				type : 'POST',
				headers: getTokenHeader(),
				url : '/'+PATIENTPATH+'/startrecordingbaselinesp/'+patientSessionIdBaseline,
				dataType : "json",
				success : function(data) {
					if(data.code == 1){
						$("#patientArchiveIdBaseline").val(data.content);
					}
				},
				error: function() {
					alert("Sorry, some problem occured. Please refresh this page and make the recording again. ");
			    }
			});
		}else{
			
			$.ajax({
				type : 'POST',
				headers: getTokenHeader(),
				url : '/'+PATIENTPATH+'/stoprecordingbaselinesp/'+patientSessionIdBaseline,
				dataType : "json",
				success : function(data) {
					if(data.code == 1){
						window.location.href="/"+PATIENTPATH+"/host";		
				     }else{
				    	alert("Sorry, some problem occured. Please refresh this page and make the recording again. ");
				     }
				},
				error: function() {
					alert("Sorry, some problem occured. Please refresh this page and make the recording again. ");
			    }
			});
		}
	});
	
});