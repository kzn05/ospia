$(document).ready(function() {
	
	$("#submit_confirm").click(function(){
		
		var password = $("#password").val();
		var confirmPassword = $("#confirmPassword").val();
		var patientid = $("#patientId").val();
		
		if(password.length < 6){
			alert("Please make sure our new password is at least 6 characters or numbers");
		}
		
		if(password == confirmPassword){
			
			var formdata = {"patientid" : patientid, "password":password};
			
			$.ajax({
				type : 'POST',
				headers: getTokenHeader(),
				url : '/'+PATIENTPATH+'/confirmNewPwd',
				contentType : 'application/json',
				dataType : "json",
				data : JSON.stringify(formdata),
				success : function(data) {
					if(data!==null){
						if(data.code==1){
							alert(data.content);
							window.location.assign(PATIENTPATH+"/ulogin_sp");
						}else{
							alert(data.content);
						}
					}
				}
			});
			
		}else{
			alert("The confirmed password is not same as the new password.");
		}
		
	});
});


