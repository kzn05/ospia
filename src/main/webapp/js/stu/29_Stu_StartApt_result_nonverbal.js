
//---------------for the turn-taking timeline: start---------------------
var timeline = null;
var data = null;
var order = 1;
var truck = 1;

var timeline_turntaking = null;
var timeline_headmovment = null;
var timeline_selftouchmovment = null;
var lastUpdatedTimeForTurnTakingWindow = 0;
var currentReflectionSection = -1;
var currentReflectionStartTime = -1;

//for feedback notes
var titlingObv = 30;
var movementObv = 0.3;
var movementTop = 50;

google.load("visualization", "1");

// Set callback to run when API is loaded
google.setOnLoadCallback(drawVisualization);

// Called when the Visualization API is loaded.
function drawVisualization() {
	
	// get the length of the video
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/totallength/'+video_name,
		dataType : "json",
		success : function(data) {
			 // right now we set the length of time as a fixed number, we need to find out the real numbmer
			duration_length = Math.ceil(data);
			var initTimeStr ='2015-01-01 00:00:00';
		    initTimeStr = initTimeStr.replace(/-/g,"/");
		    var initTime = new Date(initTimeStr);
		    var initLength = new Date(initTime.getTime() + duration_length* 1000);
			
		    var visibleStartTimeStr ='2015-01-01 00:00:00';
		    var visibleStopTimeStr ='2015-01-01 00:01:00';
		    var visibleStartTime = new Date(2015,0,1,0,0,0,0);
		    var visibleStopTime = new Date(2015,0,1,0,1,0,0);
		    
		    // specify options
		    var options = {
				width:  "100%",
		        // height: heightInPixel,
		        height: "auto",
		        layout: "box",
		        axisOnTop: true,
		        eventMargin: 10,  // minimal margin between events
		        eventMarginAxis: 0, // minimal margin between events and the axis
		        // editable: true,
		        showNavigation: false,
		        max: initLength,
		        min: initTime,
		        showCustomTime: true
		        // cluster: true,
		    };

		    /* time line of turn taking */
		    // Instantiate our timeline object.
		    timeline_turntaking = new links.Timeline(document.getElementById('turntaking'), options);
		    
		    // Create and populate a data table.
		    data_tt = new google.visualization.DataTable();
		    data_tt.addColumn('datetime', 'start');
		    data_tt.addColumn('datetime', 'end');
		    data_tt.addColumn('string', 'content');
		    data_tt.addColumn('string', 'group');
		    data_tt.addColumn('string', 'className');
		    data_tt.addColumn('string', 'id');
		    
		    // add doctor speaking period data
		    var endPoints = endPoints_doctor.split(";");
		    for(var i = 0; i<endPoints.length;i++){
		    	var points = endPoints[i].split(",");
		    	var startTime = new Date(initTime.getTime() + parseFloat(points[0])* 1000);
				var stopTime = new Date(initTime.getTime() + parseFloat(points[1])* 1000);
				var duration = parseFloat(points[1])-parseFloat(points[0]);
				data_tt.addRow([startTime, stopTime,duration.toFixed(2)+"s", "You", "doctor", 166+""]);
		    }
		    
		    // add patient speaking period data
		    var endPoints = endPoints_patient.split(";");
		    for(var i = 0; i<endPoints.length;i++){
		    	var points = endPoints[i].split(",");
		    	var startTime = new Date(initTime.getTime() + parseFloat(points[0])* 1000);
				var stopTime = new Date(initTime.getTime() + parseFloat(points[1])* 1000);
				var duration = parseFloat(points[1])-parseFloat(points[0]);
				data_tt.addRow([startTime, stopTime,duration.toFixed(2)+"s", "SP", "patient", 166+""]);
		    }
		    
		    // Draw our timeline with the created data and options
			timeline_turntaking.draw(data_tt);
			timeline_turntaking.setVisibleChartRange(visibleStartTime, visibleStopTime,true);
		    
		    google.visualization.events.addListener(timeline_turntaking, 'select',
		            function (event) {
				    	var row = getSelectedRow(timeline_turntaking);
				    	var startTime = data_tt.getValue(row, 0);
				
				    	var hours = startTime.getHours();   
				    	var minutes = startTime.getMinutes();   
				    	var seconds = startTime.getSeconds(); 
				    	var ms = startTime.getMilliseconds();  
				    	var totalSec = 60*60*hours + 60*minutes+seconds+ms/1000;
				    	jumpTo(totalSec);
				    	timeline_turntaking.setSelection();
		            }
		    );
		    google.visualization.events.addListener(timeline_turntaking, 'edit',
		            function() {
		            }
		    );
		    google.visualization.events.addListener(timeline_turntaking, 'change',
		            function() {
		            }
		    );
		    google.visualization.events.addListener(timeline_turntaking, 'add',
		            function() {
		            }
		    );
		    
		    google.visualization.events.addListener(timeline_turntaking, 'rangechange', 
		    		function() {
//		    			var myvideo = document.getElementById("video");
//		    			myvideo.pause();
//				        var range = timeline_turntaking.getVisibleChartRange();
//				        var windowStartTime = range.start;
//				        
//				        var munites = windowStartTime.getMinutes();
//				        var seconds = windowStartTime.getSeconds();
//				        
//				    	myvideo.currentTime=munites*60+seconds;
				    }
		    );
		    
		    /* time line of head movement */
		    timeline_headmovment = new links.Timeline(document.getElementById('headmovment'), options);

		    // Create and populate a data table.
		    data_hm = new google.visualization.DataTable();
		    data_hm.addColumn('datetime', 'start');
		    data_hm.addColumn('datetime', 'end');
		    data_hm.addColumn('string', 'content');
		    data_hm.addColumn('string', 'group');
		    data_hm.addColumn('string', 'className');
		    data_hm.addColumn('string', 'id');

			var noddings = noddingline.split(",");
			var shakings = shakingline.split(",");
			
			if(!(noddings.length==1 && noddings[0]=="")){
				for(var i = 0; i<noddings.length;i++){
					var temps = noddings[i].split(";");
					
					var startTime = new Date(initTime.getTime() + parseFloat(temps[1])* 1000);
					var stopTime = new Date(initTime.getTime() + parseFloat(temps[0])* 1000);
					var duration = parseFloat(temps[0])- parseFloat(temps[1]);
//					var moveValue = 
					data_hm.addRow([startTime, stopTime, duration.toFixed(2)+"s", "nodding", "doctor", 166+""]);
				}
			}else{
				// data_hm.addRow([0, 0, "1s", "nodding", "doctor", 166+""]);
			}
			if(!(shakings.length==1 && shakings[0]=="")){
				for(var i = 0; i<shakings.length;i++){
					var temps = shakings[i].split(";");
					
					var startTime = new Date(initTime.getTime() + parseFloat(temps[1])* 1000);
					var stopTime = new Date(initTime.getTime() + parseFloat(temps[0])* 1000);
					var duration = parseFloat(temps[0])- parseFloat(temps[1]);
					data_hm.addRow([startTime, stopTime,duration.toFixed(2)+"s", "shaking", "patient", 166+""]);
				}
			}

			// Draw our timeline with the created data and options
			timeline_headmovment.draw(data_hm);

		    google.visualization.events.addListener(timeline_headmovment, 'select',
		            function (event) {
				    	var row = getSelectedRow(timeline_headmovment);
				    	var startTime = data_hm.getValue(row, 0);
				
				    	var hours = startTime.getHours();   
				    	var minutes = startTime.getMinutes();   
				    	var seconds = startTime.getSeconds(); 
				    	var ms = startTime.getMilliseconds();  
				    	var totalSec = 60*60*hours + 60*minutes+seconds+ms/1000;
				    	jumpTo(totalSec);
				    	timeline_headmovment.setSelection();
		            }
		    );
		    google.visualization.events.addListener(timeline_headmovment, 'edit',
		            function() {
		            }
		    );
		    google.visualization.events.addListener(timeline_headmovment, 'change',
		            function() {
		            }
		    );
		    google.visualization.events.addListener(timeline_headmovment, 'add',
		            function() {
		            }
		    ); 
		    
//		    /* time line of head movement */
//		    timeline_selftouchmovment = new links.Timeline(document.getElementById('selftouchmovment'), options);
//
//		    // Create and populate a data table.
//		    data_st = new google.visualization.DataTable();
//		    data_st.addColumn('datetime', 'start');
//		    data_st.addColumn('datetime', 'end');
//		    data_st.addColumn('string', 'content');
//		    data_st.addColumn('string', 'group');
//		    data_st.addColumn('string', 'className');
//		    data_st.addColumn('string', 'id');
//		    
//		    var touches = selftouchline.split(",");
//			
//		    $("#selftouchTimes").text("Total Times: "+touches.length+" times");
//		    
//			if(!(touches.length==1 && touches[0]=="")){
//				for(var i = 0; i<touches.length;i++){
//					var temps = touches[i].split(",");
//					
//					var startTime = new Date(initTime.getTime() + parseFloat(temps[0])* 1000);
//					var stopTime = new Date(initTime.getTime() + parseFloat(temps[0])* 1000+1233);
//					var duration = 1.233;
//					data_st.addRow([startTime, stopTime, duration.toFixed(2)+"s", "Hand-over-face", "doctor", 166+""]);
//				}
//			}else{
//				// data_st.addRow([0, 0, "1s", "nodding", "doctor", 166+""]);
//			}
//			
//		    // Draw our timeline with the created data and options
//			timeline_selftouchmovment.draw(data_st);
//
//		    google.visualization.events.addListener(timeline_selftouchmovment, 'select',
//		            function (event) {
//				    	var row = getSelectedRow(timeline_selftouchmovment);
//				    	var startTime = data_st.getValue(row, 0);
//				
//				    	var hours = startTime.getHours();   
//				    	var minutes = startTime.getMinutes();   
//				    	var seconds = startTime.getSeconds(); 
//				    	var ms = startTime.getMilliseconds();  
//				    	var totalSec = 60*60*hours + 60*minutes+seconds+ms/1000;
//				    	jumpTo(totalSec);
//		            }
//		    );
//		    google.visualization.events.addListener(timeline_selftouchmovment, 'edit',
//		            function() {
//		            }
//		    );
//		    google.visualization.events.addListener(timeline_selftouchmovment, 'change',
//		            function() {
//		            }
//		    );
//		    google.visualization.events.addListener(timeline_selftouchmovment, 'add',
//		            function() {
//		            }
//		    ); 
		    

		}
	});
	
}

function getSelectedRow(timeline) {
    var row = undefined;
    var sel = timeline.getSelection();
    if (sel.length) {
        if (sel[0].row != undefined) {
            row = sel[0].row;
        }
    }
    return row;
}
//---------------for the turn-taking timeline: end---------------------


var OldInterval = 10;
var NewInterval = 3;

var speakPercentageChart = null;
function DynamicChangeTickIntervalSpeakPercentageChart(interval) {
	speakPercentageChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsSpeakPercentageChart() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalSpeakPercentageChart(OldInterval);
        this.zoom();
    }
    });
}


var smileChart = null;
function DynamicChangeTickIntervalSmile(interval) {
	smileChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsSmile() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalSmile(OldInterval);
        this.zoom();
    }
    });
}


var leaningChart = null;
function DynamicChangeTickIntervalLeaning(interval) {
	leaningChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsLeaning() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalLeaning(OldInterval);
        this.zoom();
    }
    });
}

var tiltingChart = null;
function DynamicChangeTickIntervalTilting(interval) {
	tiltingChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsTilting() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalTilting(OldInterval);
        this.zoom();
    }
    });
}

var frownChart = null;
function DynamicChangeTickIntervalFrown(interval) {
	frownChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsFrown() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalFrown(OldInterval);
        this.zoom();
    }
    });
}


var nofaceChart = null;
function DynamicChangeTickIntervalNoface(interval) {
	nofaceChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsNoface() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalNoface(OldInterval);
        this.zoom();
    }
    });
}

var highchartsDivWidth = 0; 
var highchartsDivHeight = 0;

var loudnessChart = null;
function DynamicChangeTickIntervalLoudness(interval) {
	loudnessChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsLoudness() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalLoudness(OldInterval);
        this.zoom();
    }
    });
}

var pitchChart = null;
function DynamicChangeTickIntervalPitch(interval) {
	loudnessChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsPitch() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalPitch(OldInterval);
        this.zoom();
    }
    });
}


var touchesChart = null;
function DynamicChangeTickIntervalTouches(interval) {
	touchesChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsTouches() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalTouches(OldInterval);
        this.zoom();
    }
    });
}


var movesChart = null;
function DynamicChangeTickIntervalMoves(interval) {
	movesChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsMoves() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalMoves(OldInterval);
        this.zoom();
    }
    });
}

var attentionChart = null;
function DynamicChangeTickIntervalAttention(interval) {
	attentionChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsAttention() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalAttention(OldInterval);
        this.zoom();
    }
    });
}

var stretchChart = null;
function DynamicChangeTickIntervalStretch(interval) {
	stretchChart.xAxis[0].update({
        tickInterval: interval
    });
}

function ExtendHighchartsStretch() {
    Highcharts.extend(Highcharts.Chart.prototype, { zoomOut: function () {
    	DynamicChangeTickIntervalStretch(OldInterval);
        this.zoom();
    }
    });
}


var speechPercentageData = null;
var loudnessData = null;
var pitchData = null;
var smileData = null;
var frownData = null;
var leaningData = null;
var tiltingData = null;
var touchesData = null;
var movesData = null;
var attentionData = null;
var stretchData = null;

function displaySpeechPercentage(plotLines){
	
	var splitLength = parseInt($("#speakpercentagesplit").val())*60;
	
	// get the speech percentage
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/speechpercentage/'+video_name+"/"+splitLength,
		dataType : "json",
		success : function(data) {
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        },
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                }
		    });  
			
			var chart;  
		    chart = new Highcharts.Chart({  
				chart: {  
					renderTo: 'speechpercentagecontainer', 
		            type: 'spline',  
		            marginRight: 10,
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            events: {
                        selection: function () {
                        	DynamicChangeTickIntervalSpeakPercentageChart(NewInterval);
                        }
                    }
		        },  
		        title: {  
		            text: '<b>Speaking percentage</b>'  
		        },  
		        xAxis: {  
		            type: 'int',  
		            tickInterval: OldInterval,
		            plotLines:plotLines,
		        },  
		        yAxis: {  
		            title: {  
		                text: 'percent'  
		            },  
		        },  
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        tooltip: {  
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y*100, 2)+"%"
		                    ;  
		            }  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        legend: {
		            layout: 'vertical',
		            align: 'right',
		            verticalAlign: 'top',
		            borderWidth: 0
		        }, 
		        
		        exporting: {  
		            enabled: false  
		        },  
		        
		        series: [
		        {  
		            name: 'You',
		            lineWidth: 2,
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                for (i = 1; i < data.length; i++) {
		                	if(data[i].doctorPercentage+data[i].patientPercentage == 0){
		                		drawdata.push({  
			                        x: data[i].time,  
			                        y: 0.5  
			                    });  
		                	}else{
		                		drawdata.push({  
			                        x: data[i].time,  
			                        y: data[i].doctorPercentage/(data[i].doctorPercentage+data[i].patientPercentage)  
			                    });  
		                	}
		                }
		                speechPercentageData = drawdata;
		                return drawdata;  
		            })()  
		        },
		        {  
		            name: 'SP',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                for (i = 1; i < data.length; i++) {
		                	
		                	if(data[i].doctorPercentage+data[i].patientPercentage==0){
		                		drawdata.push({  
			                        x: data[i].time,  
			                        y: 0.5  
			                    });  
		                	}else{
		                		drawdata.push({  
			                        x: data[i].time,  
			                        y: data[i].patientPercentage/(data[i].doctorPercentage+data[i].patientPercentage) 
			                    });   
		                	}
		                }
		                return drawdata;  
		            })()  
		        }]
			},function (chartObj) {
				speakPercentageChart = chartObj;
            });
		    ExtendHighchartsSpeakPercentageChart();
		}
	});
}

//draw sound properties: loudness, pitch, speech rate
function drawsoundProperty(totalLength, totalFrame){
	
	// get the loudness
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/loudness/'+video_name,
		dataType : "json",
		success : function(data) {
			
		    totalLength = data.totalLength;
			var loudness= data.loudness;
			var tags = loudness.split(",");
			var tagsLength = tags.length;
			var timeslot = totalLength/tagsLength;
			var endPoints = endPoints_doctor.split(";");
			var periods = new Array();
			for(var i = 0; i<endPoints.length; i++){
		    	var points = endPoints[i].split(",");
		    	var startTime = parseFloat(points[0]);
				var stopTime =  parseFloat(points[1]);
				var maxLoudness = 0;
				
				for (var j= 0; j < tagsLength; j++) {
					var currentFrame = j * timeslot;
					var currentLoudness = new Number(tags[j]);
                	var currentLoudnessValue = currentLoudness.valueOf();
					
					if(currentFrame>=startTime && currentFrame<=stopTime && maxLoudness<currentLoudnessValue){
						maxLoudness=currentLoudnessValue;
					}
				}
				periods[i] = {start:startTime, stop:stopTime, loudnessValue: maxLoudness};
		    }
			
			var totalTimes = 0;
			var specialMoment = new Array();
			for(var k = 0; k<periods.length-1; k++){
				var curValue = periods[k].loudnessValue;
				var nextValue = periods[k+1].loudnessValue;
				if(curValue>30 && curValue<nextValue && (Math.abs(nextValue-curValue)/curValue)>0.5){
					// console.log("start time" + periods[k].start + ", stop time: " + periods[k].stop + ", value: "+nextValue);
					specialMoment[totalTimes] = periods[k].start;
					totalTimes ++;
				}
			}
			var specialMomentContentLoudness="";
            if(specialMoment.length==0){
            	specialMomentContentLoudness ="<b>Notes</b><br/>We do not found any moments you obvioursly raised your voice volume.";
            }else{
            	specialMomentContentLoudness = "<b>Notes</b><br/>We found "+totalTimes +" times raised your voice volume, and they are at following timestamps: ";
            	 for (var i = 0; i < specialMoment.length; i++){
            		 specialMomentContentLoudness += " &nbsp;<a onclick='jumpTo("+(specialMoment[i]-1)+")'>"+convertSecondToMinute(specialMoment[i])+"</a>"; 
                 }
            }
            $("#specialMomentDivLoudness").html(specialMomentContentLoudness);
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        },
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                }
		    });  
		    var chart;  
		    
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'loudnesscontainer',  
		            type: 'area',  
		            marginRight: 10, 
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            events: {
                        selection: function () {
                        	DynamicChangeTickIntervalLoudness(NewInterval);
                        }
                    }
		        },  
		        plotOptions: {
		        	 spline: {
                         turboThreshold: 100000
		        	 },
		        	 area: {
		        		    turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        title: {  
		            text: '<b>Sound power </b>'  
		        },  
		        xAxis: {  
		            type: 'int', 
		            tickInterval: OldInterval
		            // tickPixelInterval: 150  
		        },  
		        yAxis: {  
		            title: {  
		                text: 'decibel'  
		            },  
		            plotBands: [{
		            	color: '#FCFFC5',
		            	from: loudnessBottom, 
		                to: loudnessTop,
//		                label: {
//		                    text: 'average',
//		                    align: 'left',
//		                    x: -45,
//		                    style: {
//		                        color: '#606060'
//		                    }
//		                }
		            }],  
		        },  
		        tooltip: { 
		        	enabled: false,
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Sound power',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;
		                var totalNum = 0;
		                var totalVolume = 0;
		                var interval = 10;
		                var validNum = 0;
		                var prevValue = 0;
		                var maxValue = 0;
		                if(tagsLength>10000 && tagsLength<20000){
		                	interval = 20;
		                }else if(tagsLength>20000 && tagsLength<40000){
		                	interval = 50;
		                }else{
		                	interval = 100;
		                }
		                
		                for (i = 0; i < tagsLength; i=i+1) {
		                	
		                	totalNum++;
		                	
		                	var number = new Number(tags[i]);
		                	var numberValue = number.valueOf();
		                	
//		                	if(numberValue>maxValue){
//		                		maxValue = numberValue;
//		                	}
//		                	if(totalNum%interval==0){
//	                			drawdata.push({  
//			                        x: timeslot*(i),
//			                        y: maxValue
//			                    });	
//		                		maxValue = 0;
//		                	}
		                	
		                	
		                	if(totalNum==interval){
		                		if(validNum==0){
		                			drawdata.push({  
				                        x: timeslot*(i-interval),
				                        y: 0
				                    });	
		                		}else{
		                			if(totalVolume == 0){
		                				drawdata.push({  
					                        x: timeslot*(i-interval),
					                		y: 0
					                    });
		                			}else{
		                				drawdata.push({  
					                        x: timeslot*(i-interval),
					                		y: totalVolume/validNum
					                    });	
		                			}
		                				
		                			prevValue = totalVolume/validNum;
		                		}
		                		
		                		totalNum = 0;
		                		totalVolume = 0;
		                		validNum=0;
		                	}else{
		                		if(numberValue>5 && numberValue<100){
		                			totalVolume = totalVolume + numberValue;
		                			validNum++;
		                		}
		                	}
		                }
		                loudnessData = drawdata;
 		                return drawdata;  
		            })()  
		        }],
		       
		    }, function (chartObj) {
		    	loudnessChart = chartObj;
            });
            ExtendHighchartsLoudness();
            
		}
	});
	
	
	// get the pitch
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/pitch/'+video_name,
		dataType : "json",
		success : function(data) {
			
			var pitch= data.pitch;
			var tags = pitch.split(",");
			var tagsLength = tags.length;
			var timeslot = totalLength/tagsLength;
			var endPoints = endPoints_doctor.split(";");
			var periods = new Array();
			for(var i = 0; i<endPoints.length; i++){
		    	var points = endPoints[i].split(",");
		    	var startTime = parseFloat(points[0]);
				var stopTime =  parseFloat(points[1]);
				var maxLoudness = 0;
				
				for (var j= 0; j < tagsLength; j++) {
					var currentFrame = j * timeslot;
					var currentLoudness = new Number(tags[j]);
                	var currentLoudnessValue = currentLoudness.valueOf();
					
					if(currentFrame>=startTime && currentFrame<=stopTime && maxLoudness<currentLoudnessValue){
						maxLoudness=currentLoudnessValue;
					}
				}
				periods[i] = {start:startTime, stop:stopTime, loudnessValue: maxLoudness};
		    }
			
			var totalTimes = 0;
			var specialMoment = new Array();
			for(var k = 0; k<periods.length-1; k++){
				var curValue = periods[k].loudnessValue;
				var nextValue = periods[k+1].loudnessValue;
				if(curValue>30 && curValue<nextValue && (Math.abs(nextValue-curValue)/curValue)>0.5){
					specialMoment[totalTimes] = periods[k].start;
					totalTimes ++;
				}
			}
			
			var specialMomentContentPitch="";
            if(specialMoment.length==0){
            	specialMomentContentPitch ="<b>Notes</b><br/>We do not found any moments you obvioursly raised your voice pitch.";
            }else{
            	specialMomentContentPitch = "<b>Notes</b><br/>We found "+totalTimes +" times raised your voice pitch, and they are at following timestamps: ";
	        	 for (var i = 0; i < specialMoment.length; i++){
	        		 specialMomentContentPitch += " &nbsp;<a onclick='jumpTo("+(specialMoment[i]-1)+")'>"+convertSecondToMinute(specialMoment[i])+"</a>"; 
	             }
            }
            $("#specialMomentDivPitch").html(specialMomentContentPitch);
			
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        },
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                }
		    });  
		    var chart;  
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'pitchcontainer',  
		            type: 'area',  
		            marginRight: 10,
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            events: {
                        selection: function () {
                        	DynamicChangeTickIntervalPitch(NewInterval);
                        }
                    }
		        },  
		        title: {  
		            text: '<b>Sound pitch</b>'  
		        },
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 },
		        	 area: {
		        		    turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        xAxis: {  
		            type: 'int',
		            tickInterval: OldInterval
		            // tickPixelInterval: 150  
		        },  
		        yAxis: {  
		            title: {  
		                text: 'semitone'  
		            },  
		            plotBands: [{
		            	color: '#FCFFC5',
		            	from: pitchBottom, 
		                to: pitchTop,
//		                label: {
//		                    text: 'average',
//		                    align: 'left',
//		                    x: -45,
//		                    style: {
//		                        color: '#606060'
//		                    }
//		                }
		            }],  
		        },  
		        tooltip: {  
		        	enabled: false,
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Sound pitch',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                var totalNum = 0;
		                var totalVolume = 0;
		                var interval = 10;
		                var validNum = 0;
		                var prevValue = 0;
		                
		                if(tagsLength>10000){
		                	interval = 50;
		                }
		                
		                if(tagsLength<200){
		                	interval = 1;
		                }
		                
		                for (i = 0; i < tagsLength; i++) {
		                	totalNum++;
		                	var number = new Number(tags[i]);
		                	var numberValue = number.valueOf();
		                	if(totalNum==interval){
		                		if(validNum==0){
		                			drawdata.push({  
				                        x: timeslot*(i-interval),
				                        y: 0
				                    });	
		                		}else{
		                			prevValue = totalVolume/validNum;
		                			if(totalVolume==0){
		                				drawdata.push({  
					                        x: timeslot*(i-interval),
					                		y: 0
					                    });
		                			}else{
		                				drawdata.push({  
					                        x: timeslot*(i-interval),
					                		y: totalVolume/validNum
					                    });
		                			}
		                				
		                		}
		                		
		                		totalNum = 0;
		                		totalVolume = 0;
		                		validNum=0;
		                	}else{
		                		if(numberValue<70){
		                			totalVolume = totalVolume + numberValue;
		                			validNum++;
		                		}
		                	}
		                	
		                }
		                drawdata[0] =drawdata[1];
		                pitchData = drawdata;
 		                return drawdata;  
		            })()  
		        }]  
		    }, function (chartObj) {
		    	pitchChart = chartObj;
            });
            ExtendHighchartsPitch();
		      
		}
	});
	
	
	
//	// get the speech rate
//	$.ajax({
//		type : 'POST',
//		headers: getTokenHeader(),
//		url : '/speechrate/'+video_name,
//		dataType : "json",
//		success : function(data) {
//			
//			Highcharts.setOptions({  
//		        global: {  
//		            useUTC: false  
//		        }  
//		    });  
//		    var chart;  
//		    chart = new Highcharts.Chart({  
//		        chart: {  
//		            renderTo: 'speechratecontainer',  
//		            type: 'column',  
//		            marginRight: 10,  
//		        },  
//		        title: {  
//		            text: '<b>Speech Rate</b>'  
//		        }, 
//		        xAxis: {  
//		            type: 'int',  
//		            // tickPixelInterval: 150  
//		        },  
//		        yAxis: {  
//		            title: {  
//		                text: 'syllable/minute'  
//		            },  
//		            plotLines: [{  
//		                value: 0,  
//		                width: 1,  
//		                color: '#808080'  
//		            }]  
//		        },  
//		        tooltip: {  
//		            formatter: function() {  
//		                    return '<b>'+ this.series.name +'</b><br/>'+ 
//		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
//		                    "value: "+Highcharts.numberFormat(this.y, 2)
//		                    ;  
//		            }  
//		        },  
//		        plotOptions: {
//		            column: {
//		                pointPadding: -0.3,
//		                borderWidth: 0,
//		                turboThreshold: 100000
//		            }
//		        },
//		        legend: {  
//		            enabled: false  
//		        },  
//		        exporting: {  
//		            enabled: false  
//		        },  
//		        series: [{  
//		            name: 'Speech Rate',
//		            events:{
//		            	click:function(event){
//		            		jumpTo(event.point.x);
//		            	}
//		            },
//		            data: (function() {  
//		                var drawdata = [],i;
//		                for (i = 0; i < data.length; i++) {
//		                	drawdata.push({  
//		                        x: data[i].time,  
//		                        y: data[i].rate  
//		                    });  
//		                }
//		                drawdata =sparseArray(drawdata); 
// 		                return drawdata; 
//		            })()  
//		        }]  
//		    });  
//		}
//	});
}

function displaySmile(){
	// get the smiles
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/smiles/'+video_name,
		dataType : "json",
		success : function(data) {
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                } 
		    });  
		    var chart;  
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'smilecontainer',  
		            type: 'area',  
		            marginRight: 10, 
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            events: {
                        selection: function () {
                        	DynamicChangeTickIntervalSmile(NewInterval);
                        }
                    }
		        },  
		        title: {  
		            text: '<b>Smile intensity</b>'  
		        },  
		        xAxis: {  
		            type: 'int',  
		            tickInterval: OldInterval 
		        }, 
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 },
		        	 area: {
		        		    turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        yAxis: {  
		            title: {  
		                text: 'intensity'  
		            }, 
		            plotLines:[{
		            	color: 'red', // Color value
		                dashStyle: 'longdash', // Style of the plot line. Default to solid
		                value: 50, // Value of where the line will appear
		                width: 2 // Width of the line
					}]	
		        },  
		        tooltip: {  
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        }, 
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Smile intensity',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                for (i = 0; i < data.length; i++) {
		                	drawdata.push({  
		                        x: data[i].tagId,  
		                        y: data[i].value
		                        // y: 0
		                    });  
		                }
		                smileData = drawdata;
		                return drawdata;  
		            })()  
		        }, {
					name:'',
					tyle:'scatter',
					marker: {
						enabled:false
					},
					data:[50]
					
				}]  
		    }, function (chartObj) {
		    	smileChart = chartObj;
            });
            ExtendHighchartsSmile();
              
		}
	});
}

function displayFrown(){
	// get the frowns
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/frown/'+video_name,
		dataType : "json",
		success : function(data) {
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
	                resetZoom: "Return",
	                resetZoomTitle: "Return to initial"
	            } 
		    });  
		    var chart;  
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'frowncontainer',  
		            type: 'area',  
		            marginRight: 10,  
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            events: {
	                    selection: function () {
	                    	DynamicChangeTickIntervalFrown(NewInterval);
	                    }
	                }
		        },  
		        title: {  
		            text: '<b>Frown intensity</b>'  
		        },  
		        xAxis: {  
		            type: 'int',  
		            tickInterval: OldInterval   
		        }, 
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 },
		        	 area: {
		        		    turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        yAxis: {  
		            title: {  
		                text: 'intensity'  
		            },  
		            plotLines:[{
		            	color: 'red', 
		                dashStyle: 'longdash',
		                value: 50, 
		                width: 2
					}]
		        },  
		        tooltip: {  
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Frown intensity',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i; 
		                
		                for (i = 0; i < data.length-25; i=i+25) {
		                	
		                	var maxForwn = 0;
		                	
		                	for(var j = 0; j < 25; j++){
		                		if(data[i+j].value>maxForwn){
		                			maxForwn += data[i+j].value;
		                		}
//		                		if(data[i+j].value>maxForwn){
//		                			maxForwn = data[i+j].value;
//		                		}
		                	}
		                	drawdata.push({  
		                        x: data[i].time,  
		                        y:maxForwn
		                    });  
		                }
		                frownData = drawdata;
		                return drawdata;  
		            })()  
		        },{
					name:'',
					tyle:'scatter',
					marker: {
						enabled:false
					},
					data:[50]
					
				}]  
		    }, function (chartObj) {
		    	frownChart = chartObj;
	        });
	        ExtendHighchartsFrown();  
		}
	});
}

function displayLeaning(){
	// get the leanings
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/leanings/'+video_name,
		dataType : "json",
		success : function(data) {
			
			var items = data;
	    	var timeslots = new Array();
	    	var timeValues = new Array();
	    	var prevValue = 0;
	    	var interval = 25;
	    	// size
	    	var tempSize = 0;
	    	// number
	    	var tempTotal = 0;
	    	var validNum=0;
	    	for(var i = 0; i < items.length; i++){
	    		
	    		tempTotal++;
	    		var values = items[i].split("\t");
	    		var faceSize = parseFloat(values[0]);
	    		
	    		if(tempTotal == interval){
	    			if(validNum==0){
	    				timeslots.push(i/25);
		    			timeValues.push(prevValue);	
	        		}else{
	        			timeslots.push(i/25);
		    			timeValues.push(tempSize/validNum);	
	        			prevValue = tempSize/validNum;
	        		}
	        		
	    			tempTotal = 0;
	    			tempSize = 0;
	        		validNum=0;
	    		}else{
	    			if(faceSize >= 5000){
	    				tempSize += faceSize;
	    				validNum++;
	    			}
	    		}
	    	}
	    	
	    	var specialMoment = [];
            for (var i = 2; i < timeslots.length-2; i++) {
            	var maxValue = 0;
            	var minValue = 1000000;
            	for (var j = -2; j < 3; j++) {
            		if(timeValues[i+j]<minValue){
            			minValue = timeValues[i+j];
            		}
            		if(timeValues[i+j]>maxValue){
            			maxValue = timeValues[i+j];
            		}
            	}
            	var percentage = (maxValue-minValue)/minValue;
            	if(percentage>0.5){
            		console.log(percentage);
            		var arrayLength = specialMoment.length;
            		
            		if(arrayLength==0){
            			specialMoment.push(i);
            		}else{
            			if(i-specialMoment[arrayLength-1]>=5){
            				specialMoment.push(i);
            			}
            		}
            	}
            }
            
            var specialMomentContentLeaning="";
            if(specialMoment.length==0){
            	specialMomentContentLeaning ="<b>System's estimation:</b><br/>We do not found any moments you obvioursly leaned your body.";
            }else{
            	
            	specialMomentContentLeaning = "<b>System's estimation:</b><br/>We found "+specialMoment.length +" times you leaned you body forward or backward, and they are at following timestamps: ";
            	 for (var i = 0; i < specialMoment.length; i++){
            		 specialMomentContentLeaning += " &nbsp;<a onclick='jumpTo("+(specialMoment[i]-1)+")'>"+convertSecondToMinute(specialMoment[i])+"</a>"; 
                 }
            }
            $("#specialMomentDivLeaning").html(specialMomentContentLeaning);
	    	
            
	    	Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                } 
		    });  
	    	
	        $('#leaningcontainer').highcharts({
	        	
	            chart: {
	            	renderTo: 'leaningcontainer',  
		            type: 'spline',  
		            marginRight: 10, 
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            events: {
                     selection: function () {
                     	DynamicChangeTickIntervalLeaning(NewInterval);
                     }
                 }
	            },
	            title: {
	                text: '<b>Body leaning (Forward & Backward)</b>'
	            },
	            xAxis: {
	            	// categories: timeslots
	            	type: 'float',
	            	tickInterval: OldInterval
	            },
	            yAxis: {
	                title: {
	                    text: 'Distance'
	                },
	                plotBands: [
//	                {
//		            	color: '#FCFFC5',
//		            	from: 14000, 
//		                to: 1000000,
//		                label: {
//		                    text: 'forward',
//		                    align: 'left',
//		                    x: -45,
//		                    style: {
//		                        color: '#606060'
//		                    }
//		                }
//		            },
		            {
		            	color: '#FCFFC5',
		            	from: leaningBottom, 
		                to: leaningTop,
//		                label: {
//		                    text: 'average',
//		                    align: 'left',
//		                    x: -45,
//		                    style: {
//		                        color: '#606060'
//		                    }
//		                }
		            }]  
	            },
	            
	            legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },
		        credits: {  
		            enabled: false  
		        },  
//	            tooltip: {
//	                enabled: true,
//	                formatter: function() {
//	                    return '<b>'+ this.series.name +'</b><br>'+this.x +': '+ this.y +'°C';
//	                }
//	            },
	            plotOptions: {
	                line: {
	                    dataLabels: {
	                        enabled: false
	                    },
	                    enableMouseTracking: true
	                },
	                spline: {
                        turboThreshold: 100000
		        	 }
	            },
	            annotationsOptions: {
		            enabledButtons: false   
		        },
	            series: [{ 
	            	lineWidth:1,
	            	name: 'Leaning',
	            	events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                for (i = 0; i < timeslots.length; i++) {
		                	
		                	if(timeValues[i]==0){
		                		drawdata.push({  
			                        x: timeslots[i],  
			                        y: null  
			                    });
		                	}else{
		                		drawdata.push({  
			                        x: timeslots[i],  
			                        y: Math.floor(timeValues[i])  
			                    });	
		                	}
		                }
		                leaningData = drawdata;
		                return drawdata;  
		            })()  
		        }]  
	            
	        }, function (chartObj) {
		    	leaningChart = chartObj;
            });
            ExtendHighchartsLeaning();
		}
	});
}


function displayTilting(){
	// get the tiltings
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/tiltings/'+video_name,
		dataType : "json",
		success : function(data) {
			
			var items = data;
			var timeslots = new Array();
			var timeValues = new Array();
			
			for(var i = 0; i < items.length; i=i+25){
				
				timeslots.push(i/25);
				
				var tiltingValue = Math.floor(parseFloat(items[i])*100)/100;
				if(Math.abs(tiltingValue)>70){
					tiltingValue = 0;
				}
				timeValues.push(tiltingValue);
			}
			
			var specialMoment = [];
			var lastMoment = -1;
            for (var i = 0; i < timeslots.length; i++) {
            	if((timeValues[i]>titlingObv||timeValues[i]<(-1*titlingObv)) && (i-lastMoment)>3){
            		specialMoment.push(i);
            		lastMoment = i;
            	}
            }
            
            var specialMomentContentTilting ="";
            if(specialMoment.length==0){
            	specialMomentContentTilting ="<b>System's estimation:</b><br/>We do not found any moments you obvioursly tilted your head.";
            }else{
            	specialMomentContentTilting ="<b>System's estimation:</b><br/>We found "+specialMoment.length +" times you obvioursly tilted your head, and they are at following timestamps: ";
                for (var i = 0; i < specialMoment.length; i++){
                	specialMomentContentTilting += " &nbsp;<a onclick='jumpTo("+(specialMoment[i]-1)+")'>"+convertSecondToMinute(specialMoment[i])+"</a>"; 
                }
            }
            
            $("#specialMomentDivTilting").html(specialMomentContentTilting);
			
			
			Highcharts.setOptions({  
				global: {  
					useUTC: false  
				} ,
				lang: {
					resetZoom: "Return",
					resetZoomTitle: "Return to initial"
				} 
			}); 
			
			$('#tiltingcontainer').highcharts({
				chart: {
					renderTo: 'tiltingcontainer',  
					type: 'spline',  
					marginRight: 10, 
					zoomType: 'x',
					width: highchartsDivWidth,
					events: {
						selection: function () {
							DynamicChangeTickIntervalTilting(NewInterval);
						}
					}
				},
				title: {
					text: '<b>Head tilting</b>'
				},
				xAxis: {
					type: 'float',
					tickInterval: OldInterval 
				},
				yAxis: {
					title: {
						text: 'Tilting angle (degree)'
					},
					plotBands: [
					{
		            	color: '#FCFFC5',
		            	from: tiltingBottom, 
		                to: tiltingTop,
//		                label: {
//		                    text: 'left',
//		                    align: 'left',
//		                    x: -45,
//		                    style: {
//		                        color: '#606060'
//		                    }
//		                }
		            }
		            ] 
				},
				credits: {  
					enabled: false  
				},  
				legend: {  
					enabled: false  
				},  
				exporting: {  
					enabled: false  
				},  
//            tooltip: {
//                enabled: true,
//                formatter: function() {
//                    return '<b>'+ this.series.name +'</b><br>'+this.x +': '+ this.y +'°C';
//                }
//            },
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					},
					spline: {
						turboThreshold: 100000
					}
					
				},
				annotationsOptions: {
		            enabledButtons: false   
		        },
				series: [{  
					name: 'Tilting',
					events:{
						click:function(event){
							jumpTo(event.point.x);
						}
					},
					lineWidth:1,
					data: (function() {  
						var drawdata = [],i;  
						//timeslots.length
						for (i = 0; i < timeslots.length; i++) {
							drawdata.push({  
								x: timeslots[i],
								y: timeValues[i]
							});  
						}
						tiltingData = drawdata;
						return drawdata;  
					})()  
				}]  
			}
			, function (chartObj) {
				tiltingChart = chartObj;
			});
			ExtendHighchartsTilting();
			
		}
	});
}

function displayNoFace(){
	
	// get the leanings
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/leanings/'+video_name,
		dataType : "json",
		success : function(data) {
			var items = data;
	    	var timeslots = new Array();
	    	var timeValues = new Array();
	    	var interval = 25;
	    	// number
	    	var tempTotal = 0;
	    	var validNum=0;
	    	for(var i = 0; i < items.length; i++){
	    		
	    		tempTotal++;
	    		var values = items[i].split(",");
	    		var faceSize = parseFloat(values[4]);
	    		
	    		if(tempTotal == interval){
	    			if(validNum<23){
	    				timeslots.push(i/25);
		    			timeValues.push(1);	
	        		}else{
	        			timeslots.push(i/25);
		    			timeValues.push(0);	
	        		}
	        		
	    			tempTotal = 0;
	    			tempSize = 0;
	        		validNum=0;
	    		}else{
	    			if(faceSize != 0){
	    				validNum++;
	    			}
	    		}
	    	}
	    	Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
                    resetZoom: "Return",
                    resetZoomTitle: "Return to initial"
                } 
		    });  
	    	
	        $('#nofacecontainer').highcharts({
	        	
	            chart: {
	            	renderTo: 'nofacecontainer',  
		            type: 'spline',  
		            marginRight: 10, 
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            events: {
                     selection: function () {
                     	DynamicChangeTickIntervalNoface(NewInterval);
                     }
                 }
	            },
	            title: {
	                text: '<b>Lost Face</b>'
	            },
	            xAxis: {
	            	type: 'float',
	            	tickInterval: OldInterval
	            },
	            yAxis: {
	                title: {
	                    text: 'Distance'
	                },
	                plotLines: [{  
		                value: 0,  
		                width: 1,  
		                color: '#808080'  
		            }]  
	            },
	          
	            legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
	            plotOptions: {
	                line: {
	                    dataLabels: {
	                        enabled: false
	                    },
	                    enableMouseTracking: true
	                },
	                spline: {
                        turboThreshold: 100000
		        	 }
	            },
	            annotationsOptions: {
		            enabledButtons: false   
		        },
	            series: [{  
	            	name: 'No face detected',
	            	events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i;  
		                for (i = 0; i < timeslots.length; i++) {
		                	drawdata.push({  
		                        x: timeslots[i],  
		                        y: timeValues[i]  
		                    });  
		                }
		                return drawdata;  
		            })()  
		        }]  
	            
	            
	            
	        }, function (chartObj) {
		    	nofaceChart = chartObj;
            });
            ExtendHighchartsNoface();
		}
	});
}


function displayTouches(){
	// get the tiltings
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/touches/'+video_name,
		dataType : "json",
		success : function(data) {
			
		var timeslots = new Array();
		var timeValues = new Array();
		
		for(var i = 1; i < data.length-1; i++){
			var preTouchObj = data[i-1];
			var curTouchObj = data[i];
			var nextObj = data[i+1];
			var diff = curTouchObj.persentage - preTouchObj.persentage;
			var diffNext = curTouchObj.persentage - nextObj.persentage;
			
			timeslots.push(i/25);
			
			if(preTouchObj.persentage==0){
				timeValues.push(0);
			}else{
				if(diff<0){
					timeValues.push(0);
				}else{
					if(diff<40){
						if(diff>1 && diffNext<-1){
							timeValues.push(diff*10);
						}else{
							timeValues.push(0);
						}
					}
					
				}
			}
		}
			
			Highcharts.setOptions({  
				global: {  
					useUTC: false  
				} ,
				lang: {
					resetZoom: "Return",
					resetZoomTitle: "Return to initial"
				} 
			}); 
			
			$('#touchescontainer').highcharts({
				chart: {
					renderTo: 'touchescontainer',  
					type: 'area',  
					marginRight: 10, 
					zoomType: 'x',
					width: highchartsDivWidth,
					events: {
						selection: function () {
							DynamicChangeTickIntervalTouches(NewInterval);
						}
					}
				},
				title: {
					text: '<b>Hand Gestures</b>'
				},
				xAxis: {
					type: 'float',
					tickInterval: OldInterval 
				},
				yAxis: {
					title: {
						 text: ''
					},
					plotLines:[{
		            	color: 'red', // Color value
		                dashStyle: 'longdash', // Style of the plot line. Default to solid
		                value: 100, // Value of where the line will appear
		                width: 2 // Width of the line
					}] 
				},
				credits: {  
					enabled: false  
				},  
				legend: {  
					enabled: false  
				},  
				exporting: {  
					enabled: false  
				},  
				plotOptions: {
//					line: {
//						dataLabels: {
//							enabled: true
//						},
//						enableMouseTracking: true
//					},
//					spline: {
//						turboThreshold: 100000
//					},
					area: {
	                    fillColor: {
	                        linearGradient: {
	                            x1: 0,
	                            y1: 0,
	                            x2: 0,
	                            y2: 1
	                        },
	                        stops: [
	                            [0, Highcharts.getOptions().colors[0]],
	                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
	                        ]
	                    },
	                    marker: {
	                        radius: 2
	                    },
	                    lineWidth: 1,
	                    states: {
	                        hover: {
	                            lineWidth: 1
	                        }
	                    },
	                    threshold: null,
	                    turboThreshold: 100000
	                }
					
				},
				annotationsOptions: {
		            enabledButtons: false   
		        },
				series: [{  
					name: '',
					events:{
						click:function(event){
							jumpTo(event.point.x);
						}
					},
					lineWidth:1,
					data: (function() {  
						var drawdata = [],i,j; 
						var timeResult=[];
						var valueResult =[];
						for (i = 25; i < timeslots.length; i=i+25) {
							var tempTotal = 0;   
							for (j = 1; j <= 25 ; j=j+1){
								tempTotal += timeValues[i-j];
							}
							timeResult.push(timeslots[i-25]);
							valueResult.push(tempTotal);
						}
						//timeslots.length
						for (i = 0; i < timeResult.length; i++) {
							if(isNaN(valueResult[i])){
								drawdata.push({  
									x: timeResult[i],  
									y: 0
								});  
							}else{
								drawdata.push({  
									x: timeResult[i],  
									y: Math.floor(valueResult[i]*1000)/1000
								});  
							}
							
						}
						// drawdata = sparseArray(drawdata,25);
						touchesData = drawdata;
						return drawdata;  
					})()  
				}, {
					name:'',
					tyle:'scatter',
					marker: {
						enabled:false
					},
					data:[2]
					
				}]  
			}
			, function (chartObj) {
				touchesChart = chartObj;
			});
			ExtendHighchartsTouches();
			
		}
	});
}


function displayAttention(){
	// get the frowns
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/attention/'+video_name,
		dataType : "json",
		success : function(data) {
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
	                resetZoom: "Return",
	                resetZoomTitle: "Return to initial"
	            } 
		    });  
		    var chart;  
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'attentioncontainer',  
		            type: 'area',  
		            marginRight: 10,  
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            events: {
	                    selection: function () {
	                    	DynamicChangeTickIntervalAttention(NewInterval);
	                    }
	                }
		        },  
		        title: {  
		            text: '<b>Inattention intensity</b>'  
		        },  
		        xAxis: {  
		            type: 'int',  
		            tickInterval: OldInterval   
		        }, 
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 },
		        	 area: {
		        		    turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        
		        yAxis: {  
		            title: {  
		                text: 'intensity'  
		            },  
		            plotLines:[{
		            	color: 'red', 
		                dashStyle: 'longdash',
		                value: 50, 
		                width: 2
					}]
		        },  
		        tooltip: {  
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Inattention intensity',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i; 
		                
		                for (i = 0; i < data.length-25; i=i+25) {
		                	
		                	var maxAttention = 0;
		                	
		                	for(var j = 0; j < 25; j++){
		                		if(data[i+j].value>maxAttention){
		                			maxAttention = data[i+j].value;
		                		}
		                	}
		                	drawdata.push({  
		                        x: data[i].time,  
		                        y:maxAttention
		                    });  
		                }
		                attentionData = drawdata;
		                return drawdata;  
		            })()  
		        },{
					name:'',
					tyle:'scatter',
					marker: {
						enabled:false
					},
					data:[50]
					
				}]  
		    }, function (chartObj) {
		    	attentionChart = chartObj;
	        });
	        ExtendHighchartsAttention();  
		}
	});
}

function displayStretch(){
	// get the frowns
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/stretch/'+video_name,
		dataType : "json",
		success : function(data) {
			
			Highcharts.setOptions({  
		        global: {  
		            useUTC: false  
		        } ,
		        lang: {
	                resetZoom: "Return",
	                resetZoomTitle: "Return to initial"
	            } 
		    });  
		    var chart;  
		    chart = new Highcharts.Chart({  
		        chart: {  
		            renderTo: 'stretchcontainer',  
		            type: 'area',  
		            marginRight: 10,  
		            zoomType: 'x',
		            width: highchartsDivWidth,
		            events: {
	                    selection: function () {
	                    	DynamicChangeTickIntervalStretch(NewInterval);
	                    }
	                }
		        },  
		        title: {  
		            text: '<b>Lip Stretch</b>'  
		        },  
		        xAxis: {  
		            type: 'int',  
		            tickInterval: OldInterval   
		        }, 
		        plotOptions: {
		        	 spline: {
                        turboThreshold: 100000
		        	 },
		        	 area: {
		        		    turboThreshold: 100000,
		                    fillColor: {
		                        linearGradient: {
		                            x1: 0,
		                            y1: 0,
		                            x2: 0,
		                            y2: 1
		                        },
		                        stops: [
		                            [0, Highcharts.getOptions().colors[0]],
		                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
		                        ]
		                    },
		                    marker: {
		                        radius: 2
		                    },
		                    lineWidth: 1,
		                    states: {
		                        hover: {
		                            lineWidth: 1
		                        }
		                    },
		                    threshold: null
		                }
		        },
		        annotationsOptions: {
		            enabledButtons: false   
		        },
		        
		        yAxis: {  
		            title: {  
		                text: 'intensity'  
		            },  
		            plotLines:[{
		            	color: 'red', 
		                dashStyle: 'longdash',
		                value: 50, 
		                width: 2
					}]
		        },  
		        tooltip: {  
		            formatter: function() {  
		                    return '<b>'+ this.series.name +'</b><br/>'+ 
		                    "second: "+Highcharts.numberFormat(this.x, 2)+'</b><br/>'+
		                    "value: "+Highcharts.numberFormat(this.y, 2)
		                    ;  
		            }  
		        },  
		        legend: {  
		            enabled: false  
		        },  
		        exporting: {  
		            enabled: false  
		        },  
		        credits: {  
		            enabled: false  
		        },  
		        series: [{  
		            name: 'Lip Stretch',
		            events:{
		            	click:function(event){
		            		jumpTo(event.point.x);
		            	}
		            },
		            data: (function() {  
		                var drawdata = [],i; 
		                
		                for (i = 0; i < data.length-25; i=i+25) {
		                	
		                	var maxStretch = 0;
		                	
		                	for(var j = 0; j < 25; j++){
		                		if(data[i+j].value>maxStretch){
		                			maxStretch = data[i+j].value;
		                		}
		                	}
		                	drawdata.push({  
		                        x: data[i].time,  
		                        y:maxStretch
		                    });  
		                }
		                stretchData = drawdata;
		                return drawdata;  
		            })()  
		        },{
					name:'',
					tyle:'scatter',
					marker: {
						enabled:false
					},
					data:[50]
					
				}]  
		    }, function (chartObj) {
		    	stretchChart = chartObj;
	        });
	        ExtendHighchartsStretch();  
		}
	});
}


function displayMovements(){
	// get the tiltings
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/moves/'+video_name,
		dataType : "json",
		success : function(data) {
			
		var timeslots = new Array();
		var timeValues = new Array();
		
		for(var i = 1; i < data.length; i++){
			var preTouchObj = data[i-1];
			var curTouchObj = data[i];
			timeslots.push(curTouchObj.lineNo/25);
			
			if((curTouchObj.persentage-preTouchObj.persentage)>0.4){
				timeValues.push(0);
			}else{
				timeValues.push(curTouchObj.persentage);
			}
		}
			
		var specialMoment = [];
		var lastMoment = -1;
        for (var i = 0; i < timeslots.length; i++) {
        	if(timeValues[i]>movementObv && (timeslots[i]-lastMoment)>3){
        		specialMoment.push(timeslots[i]);
        		lastMoment = timeslots[i];
        	}
        }
        
//        var specialMomentContentMovement ="";
//        if(specialMoment.length==0){
//        	specialMomentContentMovement ="<b>System's estimation:</b><br/>We do not found any moments you had obviours movements.";
//        }else{
//        	specialMomentContentMovement ="<b>System's estimation:</b><br/>We found "+specialMoment.length +" times you obvioursly moved your body, and they are at following timestamps: ";
//            for (var i = 0; i < specialMoment.length; i++){
//            	specialMomentContentMovement += " &nbsp;<a onclick='jumpTo("+(specialMoment[i]-1)+")'>"+convertSecondToMinute(specialMoment[i])+"</a>"; 
//            }
//        }
//        $("#specialMomentDivMovement").html(specialMomentContentMovement);
		
		
			Highcharts.setOptions({  
				global: {  
					useUTC: false  
				} ,
				lang: {
					resetZoom: "Return",
					resetZoomTitle: "Return to initial"
				} 
			}); 
			
			$('#movementcontainer').highcharts({
				chart: {
					renderTo: 'movementcontainer',  
					type: 'area',  
					marginRight: 10, 
					zoomType: 'x',
					width: highchartsDivWidth,
					events: {
						selection: function () {
							DynamicChangeTickIntervalMoves(NewInterval);
						}
					}
				},
				title: {
					text: '<b>Overall movement</b>'
				},
				xAxis: {
					type: 'float',
					tickInterval: OldInterval 
				},
				yAxis: {
					title: {
						text: 'Pecentage'
					},
					plotLines:[{
		            	color: 'red', // Color value
		                dashStyle: 'longdash', // Style of the plot line. Default to solid
		                value: movementTop, // Value of where the line will appear
		                width: 2 // Width of the line
					}]	
				},
				credits: {  
					enabled: false  
				},  
				legend: {  
					enabled: false  
				},  
				exporting: {  
					enabled: false  
				},  
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					},
					spline: {
						turboThreshold: 100000
					},
					area: {
						turboThreshold: 100000,
	                    fillColor: {
	                        linearGradient: {
	                            x1: 0,
	                            y1: 0,
	                            x2: 0,
	                            y2: 1
	                        },
	                        stops: [
	                            [0, Highcharts.getOptions().colors[0]],
	                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
	                        ]
	                    },
	                    marker: {
	                        radius: 2
	                    },
	                    lineWidth: 1,
	                    states: {
	                        hover: {
	                            lineWidth: 1
	                        }
	                    },
	                    threshold: null
	                }
					
				},
				annotationsOptions: {
		            enabledButtons: false   
		        },
				series: [{  
					name: 'Percentage',
					events:{
						click:function(event){
							jumpTo(event.point.x);
						}
					},
					lineWidth:1,
					data: (function() {  
						var drawdata = [],i;  
						//timeslots.length
						for (i = 0; i < timeslots.length; i++) {
							drawdata.push({  
								x: timeslots[i],  
								y: Math.floor(timeValues[i]*1000)/1000
							});  
						}
						drawdata = sparseArray( drawdata, 5);
						movesData = drawdata;
						return drawdata;  
					})()  
				}, {
					name:'',
					tyle:'scatter',
					marker: {
						enabled:false
					},
					data:[movementTop]
					
				}]  
			}
			, function (chartObj) {
				movesChart = chartObj;
			});
			ExtendHighchartsMoves();
			
		}
	});
}

$(document).ready(function(){

	displayNonverbal(1);
	highchartsDivWidth = $("#tabDiv").css("width").slice(0,-2);
	var bigContainerWidth = $("#bigContainer").css("width").slice(0,-2);
	
	var left = $("#titleHeader").offset().left; 
	var videoDiv = $("#video");
	videoDiv.css("position","fixed");
	// videoDiv.css("left",left);
	videoDiv.css("top","355px");
	videoDiv.css("margin-bottom","355px");
	videoDiv.css("width",bigContainerWidth*0.4);
	
	var currentChart = null;
	
	// $("#tabDiv").display();
	
	displaySpeechPercentage();
	
	// get the sound property and draw the chart
	$.ajax({
		type : 'POST',
		headers: getTokenHeader(),
		url : '/'+DOCTORPATH+'/soundproperty/'+video_name,
		dataType : "json",
		success : function(data) {
			if(data!=null){
				var totalFrame = data.totalFrame;
				var totalLength = data.totalLength;
				if(totalFrame>0 && totalLength>0){
					drawsoundProperty(totalLength, totalFrame);
				}				
			}
		}
	});
	
	displaySmile();
	displayFrown();
	displayLeaning();
	displayTilting();
	// displayTouches();
	displayMovements();
	displayAttention();
	displayStretch();
	
	$("#speakpercentagesplit").change(function(){
		
		var splitLength = parseInt($("#speakpercentagesplit").val())*60;
		plotLines = new Array();
		for(var i = 1; i <= duration_length/splitLength; i++){
			var line = {
					value:splitLength*i,
					color: 'blue',
		        	dashStyle: 'solid',
		        	width:2,
		        	id: 'plot-line-'+(i+1)
				};
			plotLines.push(line);
		}
		
		displaySpeechPercentage(plotLines);
		setTimeout(function(){
			$("#nonverbal_li2").click();
			$("#nonverbal_li2").click();
			$("#nonverbal_li2").click();
		}, 1000);
		
	});
	
	$("#feedbackButton").click(function(){
		var url = window.location.href;
		var archiveId = url.split("&")[1];
		
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+DOCTORPATH+'/video/'+archiveId,
			dataType : "json",
			success : function(data) {
				if(data!=null){
					var sessionId = data.sessionId;
					window.location.href="/stu/nonverbalfeedback_stu/"+sessionId;
				}
			}
		});
	});
	
	$("#nonverbalselectbtn").click(function(){
		var answers = $("input[name='nonverbalselect']:checked");	
		var qvalue= "";
		
//		if(answers.length>3){
//			alert("You can only select 3 items.");
//			return;
//		}
		
		for (var i = 0; i < answers.length; i++) {
			if(qvalue==""){
				qvalue += answers[i].value;
			}else {
				qvalue += ","+answers[i].value;
			}
		}
		var params = window.location.href.split("/");
		var param = params[params.length-1];
		param += "&"+qvalue;
		window.open("/stu/vdetail/"+param,'_blank');
	});
	
	
	
//	$("#video").click(function(){
//		var browserId = detectBrowser();
//		var vid = document.getElementById("video");
//		if(browserId==1){
//			if(video.paused==true){
//				vid.play();
//			}else{
//				vid.pause();
//			}
//		}else{
//			if(vid.paused==true){
//				vid.play();
//			}else{
//				vid.pause();
//			}
//		}
//	});
	
	
});



function jumpTo(time){
	var myvideo = document.getElementById("video");
	myvideo.currentTime=time;
	myvideo.pause();
}


function seeDetail(detailName){
	var temp = "tr[name='"+detailName+"']";
	$(temp).toggle();
}

function sparseArray(denseArray, intervalNumber){
	
	// return denseArray;
	var returnArray = [],i;
	for (i = 0; i < denseArray.length; i=i+intervalNumber) {
	    	var temp  = denseArray[i];
	    	returnArray.push(temp);
	}
	 return returnArray;
}

var comments =notes;

function displayNonverbal(number){
	
	// ga('send','event', 'nonverbal_div_show', 'nonverbal_'+number);
	ga('send','pageview','nonverbal_'+number);
	
	drawVisualization();
	hideAllNonverbal();
	removeAllActive();
	$("#nonverbal_li"+number).attr("class","active");
	$("#nonverbal_div"+number).show();
	
	addReflectionLog(number);
	
	if(number == 2){ 
		currentChart = $("#speechpercentagecontainer").highcharts();
		drawAnnotations(currentChart, comments, speechPercentageData);
		
	}else if(number == 3){
		currentChart = $("#loudnesscontainer").highcharts();
		drawAnnotations(currentChart, comments, loudnessData);
		
	}else if(number == 4){ 
		currentChart = $("#pitchcontainer").highcharts();
		drawAnnotations(currentChart, comments, pitchData);
		
	}else if(number == 5){ 
		currentChart = $("#smilecontainer").highcharts();
		drawAnnotations(currentChart, comments, smileData);
		
	}else if(number == 6){ 
		currentChart = $("#frowncontainer").highcharts();
		drawAnnotations(currentChart, comments, frownData);
		
	}else if(number == 7){ 
		currentChart = $("#leaningcontainer").highcharts();
		drawAnnotations(currentChart, comments, leaningData);
		
	}else if(number == 8){ 
		currentChart = $("#tiltingcontainer").highcharts();
		drawAnnotations(currentChart, comments, tiltingData);
		
	}else if(number == 12){ 
		currentChart = $("#touchescontainer").highcharts();
		drawAnnotations(currentChart, comments, touchesData);
	}else if(number == 13){ 
		currentChart = $("#movementcontainer").highcharts();
		drawAnnotations(currentChart, comments, movesData);
	}else if(number == 14){ 
		currentChart = $("#attentioncontainer").highcharts();
		drawAnnotations(currentChart, comments, attentionData);
	}else if(number == 15){ 
		currentChart = $("#stretchcontainer").highcharts();
		drawAnnotations(currentChart, comments, stretchData);
	}
	
	if((number>=1 && number<=10) || number==12 || number==13 || number==14 || number==15){
		var myvideo = document.getElementById("video");
		
		myvideo.ontimeupdate = function(){
			
			var startTime = myvideo.currentTime;
			var startTimeMunite = Math.floor(startTime/60);
			var startTimeSecond = Math.floor(startTime)%60;
			
			var windowStartTime = new Date(2015,0,1,0,startTimeMunite,startTimeSecond,0);
			
			timeline_headmovment.setCustomTime(windowStartTime);
			// timeline_selftouchmovment.setCustomTime(windowStartTime);
			
			if(lastUpdatedTimeForTurnTakingWindow!=windowStartTime){
				lastUpdatedTimeForTurnTakingWindow = windowStartTime;
				var windowEndTime = new Date(2015,0,1,0,startTimeMunite+1,startTimeSecond,0);
				timeline_turntaking.setVisibleChartRange(windowStartTime, windowEndTime);
			}
			
			if((number>=2 && number<=8)||number==12||number==13||number==14||number==15){
				currentChart.xAxis[0].removePlotLine('plot-line-1');
				
				currentChart.xAxis[0].addPlotLine({
					value:myvideo.currentTime,
					color: 'red',
		        	dashStyle: 'solid',
		        	width:2,
		        	id: 'plot-line-1'
				});	
			}
		};	
	}
	
		
}
function hideAllNonverbal(){
	for (var i = 0; i < 16; i++) {
		$("#nonverbal_div"+i).hide();
	}
}

function removeAllActive(){
	for (var i = 0; i < 16; i++) {
		$("#nonverbal_li"+i).removeAttr("class");
	}
}


function drawAnnotations(currentChart, comments, currentData){

	for (var i = 0; i < comments.length; i++) {
		var comment = comments[i].comment;
		var time = comments[i].time;
		var itemInData = findNearestPoint(time, currentData);
		
		currentChart.addAnnotation({
        	xValue: itemInData.x,
        	yValue: itemInData.y,
        	title: {
        		text: 'C',
        	},
        	linkedTo: comment,
            allowDragY: false,
            allowDragX: false,
            anchorX: "center",
            anchorY: "center",
            shape: {
                type: 'circle',
                params: {
                    r: 9,
                    stroke: '#c55'
                }
            },
            events: {
            	mouseover: function(e){
            		
            	  var  tempComment = findInComment(comments,this.options.xValue);
            	  $("#reporting").html(tempComment.noteContent);
                  var left = e.pageX+10;
                  var top = e.pageY-10;
                  
                  $("#reporting").css("left",left + "px"); 
                  $("#reporting").css("top",top + "px");
                  $("#reporting").show();
            	},
            	click: function(e){
            		jumpTo(this.options.xValue);
            	},
	            mouseout: function(e){
	            	$("#reporting").hide();
	            	$("#reporting").html("");
	            }            
            	
            }
         });
	}
	
}



function findNearestPoint(time, data){
	
	var difference = 1000000;
	var result = 0;
	for (var i = 0; i < data.length; i++) {
		var timeInData = data[i].x;
		if(Math.abs(timeInData-time)<difference){
			difference = Math.abs(timeInData-time);
			result = data[i];
		}
	}
	
	return result;
}



function findInComment(comments, xValue){
	
	for (var i = 0; i < comments.length; i++) {
		var comment = comments[i];
		if(Math.abs(comment.time-xValue)<2){
			return comment;
		}
	}
	
	return null; 
}



function addReflectionLog(number){
	
	if(currentReflectionSection!=-1 && currentReflectionSection !=number && currentReflectionStartTime != -1){
		
		var reflectionLog= new Object();
		
		reflectionLog.sessionId = sessionId;
		reflectionLog.nonverbalType = currentReflectionSection;
		reflectionLog.startTime = currentReflectionStartTime;
		
		var currentTime = new Date().getTime();
		reflectionLog.endTime = currentTime;
		
		currentReflectionSection = number;
		currentReflectionStartTime = new Date().getTime();
		
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+DOCTORPATH+'/addreflectionlog',
			contentType : 'application/json',
			dataType : "json",
			data : JSON.stringify(reflectionLog),
			success : function(data) {
				if (data.code == 1) {
				} else {
				}
			}
		});
		
	}else{
		if(currentReflectionSection==-1){
			currentReflectionSection = number;
		}
		
		if(currentReflectionStartTime == -1){
			currentReflectionStartTime = new Date().getTime();
		}
	}
}



