var questionsData;


$(document).ready(function() {

	var answerId = $("#answerId").val();
	
	if(answerId==null || answerId==""){
		var surveyId = $("#surveyId").val();
		
		$.ajax({
			type : 'GET',
			url : '/'+SURVEYPATH+'/previewsurvey/' + surveyId,
			dataType : "json",
			success : function(data) {

				if (data != null) {
					
					questionsData = data;
					
					var surveyTitle = data.title;
					var surveyDescription = data.description;
					var surveyOrderOfQuestions = data.orderOfQuestions;
					var questions = data.questions;
					var questionOrders = surveyOrderOfQuestions.split(",");

					$("#surveyTitle").text(surveyTitle);
					$("#surveyDescription").text(surveyDescription);
	                
					var divId = "questions";
					var element = "<li><p style='display:none; color:red;' id='surveyError'></p></li>";
					$("#"+divId).append(element);
					
					for (var i = 0; i < questionOrders.length; i++) {
						var question = findQuestionById(questionOrders[i],
								questions);
						displayQuestion(question, divId);
					}
					
					if($("#ispreview").val()==0){
						var element = "<li><input id='saveForm' onclick='submitAnswer()' class='usyd-ui-button-primary' type='button' value='Submit' /></li>";
						$("#"+divId).append(element);
					}

				}

			}
		});
	}else{
		$.ajax({
			type : 'GET',
			url : '/survey/loadanswer/' + answerId,
			dataType : "json",
			success : function(data) {

				if (data != null) {
					
					var survey = data.survey;
					var content = data.content;
					var answers = eval(content);
					
					questionsData = survey;
					
					var surveyTitle = survey.title;
					var surveyDescription = survey.description;
					var surveyOrderOfQuestions = survey.orderOfQuestions;
					var questions = survey.questions;
					var questionOrders = surveyOrderOfQuestions.split(",");

					$("#surveyTitle").text(surveyTitle);
					$("#surveyDescription").text(surveyDescription);
	               
					var divId = "questions";
					for (var i = 0; i < questionOrders.length; i++) {
						var answerForQuestion = null;
						var question = findQuestionById(questionOrders[i],questions);
						
						for (var j = 0; j < answers.length; j++) {
							if(answers[j].qid==question.id){
								answerForQuestion = answers[j];
							}
						}
						displayQuestionWithAnswer(question, divId, answerForQuestion);
					}
					disableAllInput();
					
					// accessSecondSurvey();
					
				}
			}
		});
	}
	
	
});


function findQuestionById(id, questions) {

	for (var i = 0; i < questions.length; i++) {
		if (questions[i].id == id) {
			return questions[i];
		}
	}

	return null;
}

function displayQuestionWithAnswer(question, divId,answerForQuestion) {

	var element = "";
	if (question.typeS == "TEXTAREA") {
		element = displayTextareaWithAnswer(question, answerForQuestion);
	} else if (question.typeS == "TEXTFIELD") {
		element = displayTextfieldWithAnswer(question, answerForQuestion);
	} else if (question.typeS == "RADIO") {
		element = displayRadioWithAnswer(question, answerForQuestion);
	} else if (question.typeS == "CHECKBOX") {
		element = displayCheckboxWithAnswer(question, answerForQuestion);
	}else if (question.typeS == "LIKERT") {
		element = displayLikertWithAnswer(question, answerForQuestion);
	}
	
	$("#"+divId).append(element);
	
}

function displayTextfieldWithAnswer(question, answerForQuestion) {
	
	var element = "<li id='aaa' class='notranslate'>"
		+"<label class='desc'>"+ question.title + "</label> "
		+"<label>"+ question.description +" </label> "
		+"<div><input  id='q"+question.id+"' name='q"+question.id+"' type='text' class='field text medium' value='"+answerForQuestion.qvalue+"' maxlength='255' tabindex='28' onkeyup='' /></div>"
		+"</li><hr>";
	
	return element;
}

function displayTextareaWithAnswer(question, answerForQuestion) {
	
	var element = "<li id='aaa' class='notranslate'>"
		+"<label class='desc'>"+ question.title + "</label> "
		+"<label>"+ question.description +" </label> "
		+"<div><textarea  id='q"+question.id+"' name='q"+question.id+"' class='field textarea medium' spellcheck='true' rows='10' cols='50' tabindex='5' onkeyup=''>"+answerForQuestion.qvalue+"</textarea></div>"
		+"</li><hr>";
	
	return element;
}

function displayRadioWithAnswer(question, answerForQuestion) {
	
	qvalue = answerForQuestion.qvalue;
	var element = "<li id='foli115' class='notranslate threeColumns'>"
		+"<fieldset>"
		+"<legend id='title115' class='desc'> "+question.title+" </legend>"
		+"<label>"+ question.description +" </label> "
		+"<div>";
	
	var options = question.options;
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		var optvalue = option.optvalue;
		if(qvalue==optvalue){
			element += "<span> <input  name='q"+question.id+"' type='radio' class='field radio' checked='checked' value="+optvalue+" tabindex='8' />"
	           +"<label class='choice'>"+optkey+"</label>"
	           +"</span>";
		}else{
			element += "<span> <input  name='q"+question.id+"' type='radio' class='field radio' value="+optvalue+" tabindex='8' />"
	           +"<label class='choice'>"+optkey+"</label>"
	           +"</span>";
		}
		
	}
	
	element += "</div></fieldset></li><hr>";
	
	return element;
}

function displayCheckboxWithAnswer(question, answerForQuestion) {
	
	var options = question.options;
	
	var largestLength=0;
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		if(optkey.length>largestLength){
			largestLength = optkey.length;
		}
	}
	
	var style = "";
	if(largestLength>30){
		style = "oneColumn";
	}else{
		style = "threeColumns";
	}
	
	qvalues = answerForQuestion.qvalue.split(",");
	
	var element = "<li id='foli115' class='notranslate "+style+"'>"
		+"<fieldset>"
		+"<legend id='title115' class='desc'> "+question.title+" </legend>"
		+"<label>"+ question.description +" </label> "
		+"<div>";
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		var optvalue = option.optvalue;
		var temp="";
		for (var j = 0; j < qvalues.length; j++) {
			qvalue = qvalues[j];
			if(qvalue==optvalue){
				temp += "<span> <input  name='q"+question.id+"' type='checkbox' class='field checkbox' checked='checked' value="+optvalue+" tabindex='12' />"
		           +"<label class='choice'>"+optkey+"</label>"
		           +"</span>";
			}
		}
		
		if(temp==""){
			temp += "<span> <input  name='q"+question.id+"' type='checkbox' class='field checkbox' value="+optvalue+" tabindex='12' />"
	           +"<label class='choice'>"+optkey+"</label>"
	           +"</span>";
		}
		element += temp;
		
	}
	
	element += "</div></fieldset></li><hr>";

	return element;
}


function displayLikertWithAnswer(question, answerForQuestion){
	
	qvalue = answerForQuestion.qvalue;
	
	var options = question.options;
	
	var optionNumbers = options.length;
	var element = "<li id='foli115' class='likert notranslate col"+optionNumbers+"'>"
		+"<label>"+ question.description +" </label> "
		+"<table cellspacing='0'>"
		+"<thead><tr><th>&nbsp;</th>";
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		element += "<td>"+optkey+"</td>";
	}
	element += "</tr></thead><tbody><tr class='statement108 alt'>"
		     + "<th><label for='Field108'>"+question.title+"</label></th>";
		
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optvalue = option.optvalue;
		if(qvalue==optvalue){
			element += "<td title='"+optvalue+"'><input  name='q"+question.id+"' type='radio' tabindex='1' value='"+optvalue+"' checked='checked'/> <label>"+optvalue+"</label></td>";
		}else{
			element += "<td title='"+optvalue+"'><input  name='q"+question.id+"' type='radio' tabindex='1' value='"+optvalue+"' /> <label>"+optvalue+"</label></td>";
		}
	}
	element += "</tr></tbody></table></li><hr>";
	return element;

}


function disableAllInput(){
	$(":input").attr("disabled","disabled");
}


function submitAnswer(){
	
	
	var questions = questionsData.questions;
	var checkResult = checkAnswer(questions);
	
	if(checkResult!=""){
		$("#surveyError").text(checkResult);
		$("#surveyError").show();
	}else{
		var answers = new Array();
		
		for (var i = 0; i < questions.length; i++) {
			var question = questions[i];
			var answer = new Object();
			if (question.typeS == "TEXTAREA") {
				answer = submitText(question);
				
			} else if (question.typeS == "TEXTFIELD") {
				answer = submitText(question);
				
			} else if (question.typeS == "RADIO") {
				answer = submitSingleChoice(question);
				
			} else if (question.typeS == "CHECKBOX") {
				answer = submitMultipleChoice(question);
				
			}else if (question.typeS == "LIKERT") {
				answer = submitSingleChoice(question);
				
			}
			
			answers.push(answer);
		}
		
		
		var patientId = $("#patientId").val();
		var doctorId = $("#doctorId").val();
		var teacherId = $("#teacherId").val();
		var nextpage = $("#nextpage").val();
		var sessionId = $("#sessionId").val();
		
		var survey = new Object();
		survey.id = $("#surveyId").val();
		var answer = new Object();
		answer.survey = survey;
		answer.content = JSON.stringify(answers);
		answer.sessionId = sessionId;
		
		if(doctorId!=null && doctorId!=""){
			var doctor = new Object();
			doctor.doctorId = doctorId;
			answer.doctor = doctor;
			answer.patient = null;
			answer.teacher = null;
		}
		
		if(patientId!=null && patientId!=""){
			var patient = new Object();
			patient.patientid = patientId;
			answer.patient = patient;
			answer.doctor = null;
			answer.teacher = null;
		}
		
		if(teacherId!=null && teacherId!=""){
			var teacher = new Object();
			teacher.teacherId = teacherId;
			answer.teacher = teacher;
			answer.doctor = null;
			answer.patient = null;
		}
		
		$.ajax({
			type : 'POST',
			headers: getTokenHeader(),
			url : '/'+SURVEYPATH+'/saveanswer',
			contentType : 'application/json',
			dataType : "json",
			data : JSON.stringify(answer),
			beforeSend : function() {
				showWaitingPopUp();
			},
			success : function(data) {
				if (data.code == 1) {
					if(nextpage==null ||nextpage==""){
						alert("Submit success. Thanks for your feedback.");
						window.close();
					}else{
						window.location.assign(nextpage);
					}
					
				} else {
					alert("save fail");
				}
				cancelPopup();
			}
		});
	}
	

	
}

function checkAnswer(questions){
	var result = 0;
	for (var i = 0; i < questions.length; i++) {
		var answer=new Object();
		var question = questions[i];
		var qIsCompulsory = question.isCompulsory;
		if (question.typeS == "TEXTAREA") {
			answer = submitText(question);
		} else if (question.typeS == "TEXTFIELD") {
			answer = submitText(question);
		} else if (question.typeS == "RADIO") {
			answer = submitSingleChoice(question);
		} else if (question.typeS == "CHECKBOX") {
			answer = submitMultipleChoice(question);
		}else if (question.typeS == "LIKERT") {
			answer = submitSingleChoice(question);
		}
		if(qIsCompulsory==1 && (answer.qvalue==""||answer.qvalue==null)){
			result = 1;
		}
	}
	
	if(result==1){
		return "You have to fill out all the questions.";
	}
	return "";
	
}


function submitSingleChoice(question){
	
	var qid = question.id;
	var qIsCompulsory = question.isCompulsory;
	var qvalue = $("input[name='q"+qid+"']:checked").val();	
	
	var result = new Object;
	result.qid = qid;
	result.qvalue = qvalue;
	
	if(qIsCompulsory==1 && (qvalue==""||qvalue==null)){
		$("#error"+qid).show();
	}else{
		$("#error"+qid).hide();
	}
	
	return result;
}

function submitMultipleChoice(question){
	
	var qid = question.id;
	var qIsCompulsory = question.isCompulsory;
	var answers = $("input[name='q"+qid+"']:checked");	
	var qvalue= "";
	for (var i = 0; i < answers.length; i++) {
		
		if(qvalue==""){
			qvalue += answers[i].value;
		}else {
			qvalue += ","+answers[i].value;
		}
	}
	
	var result = new Object;
	result.qid = qid;
	result.qvalue = qvalue;
	
	if(qIsCompulsory==1 && (qvalue==""||qvalue==null)){
		$("#error"+qid).show();
	}else{
		$("#error"+qid).hide();
	}
	
	return result;
}

function submitText(question){
	var qid = question.id;
	var qIsCompulsory = question.isCompulsory;
	var qvalue = $("#q"+qid).val();
	
	var result = new Object;
	result.qid = qid;
	result.qvalue = qvalue;
	
	if(qIsCompulsory==1 && (qvalue==""||qvalue==null)){
		$("#error"+qid).show();
	}else{
		$("#error"+qid).hide();
	}
	
	return result;
}


function findQuestionById(id, questions) {

	for (var i = 0; i < questions.length; i++) {
		if (questions[i].id == id) {
			return questions[i];
		}
	}

	return null;
}

function displayQuestion(question, divId) {

	var element = "";
	if (question.typeS == "TEXTAREA") {
		element = displayTextarea(question);
	} else if (question.typeS == "TEXTFIELD") {
		element = displayTextfield(question);
	} else if (question.typeS == "RADIO") {
		element = displayRadio(question);
	} else if (question.typeS == "CHECKBOX") {
		element = displayCheckbox(question);
	}else if (question.typeS == "LIKERT") {
		element = displayLikert(question);
	}
	
	$("#"+divId).append(element);
	
}

function displayTextfield(question) {
	
	var element = "<li id='aaa' class='notranslate'>"
		+"<label class='desc'>"+ question.title + "</label> "
		+"<label>"+ question.description +" </label> "
		+"<div><input id='q"+question.id+"' name='q"+question.id+"' type='text' class='field text medium' value='' maxlength='255' tabindex='28' onkeyup='' /></div>"
		+"<label class='surveyerror' id='error"+question.id+"'>You have to fill out this question.</label> "
		+"</li>"
		+"<hr>";
	
	return element;
}

function displayTextarea(question) {
	
	var element = "<li id='aaa' class='notranslate'>"
		+"<label class='desc'>"+ question.title + "</label> "
		+"<label>"+ question.description +" </label> "
		+"<div><textarea id='q"+question.id+"' name='q"+question.id+"' class='field textarea medium' spellcheck='true' rows='10' cols='50' tabindex='5' onkeyup=''></textarea></div>"
		+"<label class='surveyerror' id='error"+question.id+"'>You have to fill out this question.</label> "
		+"</li><hr>";
	
	return element;
}

function displayRadio(question) {
	
	var element = "<li id='foli115' class='notranslate threeColumns'>"
		+"<fieldset>"
		+"<legend id='title115' class='desc'> "+question.title+" </legend>"
		+"<label>"+ question.description +" </label> "
		+"<div>";
	
	var options = question.options;
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		var optvalue = option.optvalue;
		element += "<span> <input name='q"+question.id+"' type='radio' class='field radio' value="+optvalue+" tabindex='8' />"
		           +"<label class='choice'>"+optkey+"</label>"
		           +"</span>";
	}
	
	element += "</div></fieldset>"
	+"<label class='surveyerror' id='error"+question.id+"'>You have to fill out this question.</label> "
	+"</li><hr>";
	
	return element;
}

function displayCheckbox(question) {
	var options = question.options;
	
	var largestLength=0;
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		if(optkey.length>largestLength){
			largestLength = optkey.length;
		}
	}
	
	var style = "";
	if(largestLength>30){
		style = "oneColumn";
	}else{
		style = "threeColumns";
	}
	
	var element = "<li id='foli115' class='notranslate "+style+"'>"
		+"<fieldset>"
		+"<legend id='title115' class='desc'> "+question.title+" </legend>"
		+"<label>"+ question.description +" </label> "
		+"<div>";
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		var optvalue = option.optvalue;
		element += "<span> <input name='q"+question.id+"' type='checkbox' class='field checkbox' value="+optvalue+" tabindex='12' />"
		           +"<label class='choice'>"+optkey+"</label>"
		           +"</span>";
	}
	
	element += "</div></fieldset>"+
	"<label class='surveyerror' id='error"+question.id+"'>You have to fill out this question.</label> "+
	"</li><hr>";

	return element;
}


function displayLikert(question){
	
	var options = question.options;
	
	var optionNumbers = options.length;
	var element = "<li id='foli115' class='likert notranslate col"+optionNumbers+"'>"
		+"<table cellspacing='0'>"
		+"<thead><tr><th>&nbsp;</th>";
	
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optkey = option.optkey;
		element += "<td>"+optkey+"</td>";
	}
	element += "</tr></thead><tbody><tr class='statement108 alt'>"
		     + "<th><label for='Field108'>"+question.title+"</label></th>";
		
	for (var i = 0; i < options.length; i++) {
		var option = options[i];
		var optvalue = option.optvalue;
		element += "<td title='"+optvalue+"'><input name='q"+question.id+"' type='radio' tabindex='1' value='"+optvalue+"' /> <label>"+optvalue+"</label></td>";
	}
	element += "</tr></tbody></table>" +
	"<label>"+ question.description +" </label> "+	
	"<label class='surveyerror' id='error"+question.id+"'>You have to fill out this question.</label> "+	
	"</li><hr>";
	return element;

}

function showWaitingPopUp() {
	$.blockUI({
		message : $('#waitingPopup'),
		css : {
			top : ($(window).height() - 50) / 2 + 'px',
			left : ($(window).width() - 200) / 2 + 'px',
			width : '200px',
			height : '50px',
		}
	});
}

function cancelPopup() {
	$.unblockUI();
}


