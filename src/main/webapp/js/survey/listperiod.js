$(document).ready(function() {

	validate();
});

function editPeriod(periodId) {

	$.ajax({
		type : 'POST',
		headers : getTokenHeader(),
		url : '/'  + SURVEYPATH + '/editperiod/' + periodId,
		dataType : "json",
		success : function(data) {
			if (data != null) {
				$("#id").val(data.id);
				$("#startdate").val(data.startDateStr);
				$("#enddate").val(data.endDateStr);
				$("#note").val(data.note);
				$("#submit").val("update");
			}
		}
	});

}

function clearForm() {
	$("#id").val("");
	$("#startdate").val("");
	$("#enddate").val("");
	$("#note").val("");
	$("#submit").val("save");
}

function disablePeriod(periodId) {

	if (confirm("Are you sure to delete this period?")) {
		$.ajax({
			type : 'POST',
			headers : getTokenHeader(),
			url : '/'  + SURVEYPATH + '/deleteperiod/' + periodId,
			dataType : "json",
			success : function(data) {
				if (data != null) {
					location.reload();
				}
			}
		});
	}

}

function submitForm() {

	var id = $("#id").val();
	var startdate = $("#startdate").val();
	var enddate = $("#enddate").val();
	var note = $("#note").val();

	var startTime = moment(startdate, "DD/MM/YYYY").valueOf();
	var endTime = moment(enddate, "DD/MM/YYYY").valueOf() + 3600 * 1000 * 24;

	var formdata;
	if (id != null && id != "") {
		formdata = {
			id : id,
			startDate : startTime,
			endDate : endTime,
			startDateStr : startdate,
			endDateStr : enddate,
			note : note,
			status : 1
		};
	} else {
		formdata = {
			startDate : startTime,
			endDate : endTime,
			startDateStr : startdate,
			endDateStr : enddate,
			note : note,
			status : 1
		};
	}

	$.ajax({
		type : 'POST',
		headers : getTokenHeader(),
		url : '/'  + SURVEYPATH + '/saveperiod',
		contentType : 'application/json',
		dataType : "json",
		data : JSON.stringify(formdata),
		success : function(data) {
			if (data != null) {
				if (data.code == 1) {
					alert("Opetion success!");
					location.reload();
				} else {
					alert("Opetion fail!");
				}
			}
		}
	});
}

function validate() {

	$("#periodForm").validate({
		rules : {
			startdate : {
				required : true,
				notblank : true,
				dateformat : true
			},
			enddate : {
				required : true,
				notblank : true,
				dateformat : true
			}
		},
		messages : {
			startdate : {
				required : "Please enter the start date",
				notblank : "Not a valid title",
				dateformat : "Please use the right date format: DD/MM/YYYY"
			},
			enddate : {
				required : "Please enter the end date",
				notblank : "Not a valid firstname",
				dateformat : "Please use the right date format: DD/MM/YYYY"
			}
		},

		submitHandler : function(form) {
			if ($(form).valid()) {
				submitForm();
			}
			return false;
		}

	});

}
