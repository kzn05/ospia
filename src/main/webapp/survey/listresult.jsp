<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Survey List</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/survey/listpatient.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						
						<div id="searchResultDiv" style="">
							<br>
							<h3>All SOCA results</h3>
							<p>${results.size()} students finished their sessions.</p>
							<table class="table">
								<thead>
									<tr>
										<th style="width:10%">zId</th>
										<th style="width:20%">Stu Name</th>
										<th style="width:15%">Q1</th>
										<th style="width:15%">Reason1</th>
										<th style="width:15%">Q2</th>
										<th style="width:15%">Reason2</th>
										<th style="width:15%">Q3</th>
										<th style="width:15%">Reason3</th>
										<th style="width:15%">Q4</th>
										<th style="width:15%">Reason4</th>
										<th style="width:10%">Total</th>
									</tr>
								</thead>

								<tbody>
									<c:forEach var="result" items="${results}">
										<tr>
											<td>${result.zId}</td>
											<td>${result.name}</td>
											<td>${result.q1}</td>
											<td>${result.reason1}</td>
											<td>${result.q2}</td>
											<td>${result.reason2}</td>
											<td>${result.q3}</td>
											<td>${result.reason3}</td>
											<td>${result.q4}</td>
											<td>${result.reason4}</td>
											<td>${result.total}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
