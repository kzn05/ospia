
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Edit Survey</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/survey/editsurvey.js"></script>
</head>

<body>
	<c:import url="/adminheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<input type="hidden" id="surveyId" value="${survey.id}">
						<form class="form-horizontal">
							<fieldset>

								<div class="usyd-form-component">
									<div class="control-group">
										<label class="control-label" for="textinput"></label>
										<div class="controls">
											<h3>Edit a survey</h3>
										</div>
									</div>
								</div>

								<div class="form-group" id="registerErroDiv"
									style="display:none">
									<label class="col-md-4 control-label" for="textinput"></label>
									<div class="col-md-4">
										<p style="color:red" id="registerError"></p>
									</div>
								</div>

								<div class="usyd-form-component">
									<div class="control-group">
										<label class="control-label" for="textinput">Survey
											Title</label>
										<div class="controls">
											<input id="title" name=title type="text"
												placeholder="Survey Title" value="${survey.title }">
										</div>
									</div>
								</div>

								<div class="usyd-form-component">
									<div class="control-group">
										<label class="control-label" for="textinput">Survey
											Description</label>
										<div class="controls">
											<textarea id="description" name="description" rows="10"
												cols="100">${survey.description}</textarea>
										</div>
									</div>
								</div>

								<div class="usyd-form-component">
									<div class="control-group">
										<label class="control-label" for="textinput">Questions</label>
										<div class="col-md-10">
											<table class="table">
												<thead>
													<tr>
														<th style="width:15%">Title</th>
														<th style="width:30%">Description</th>
														<th style="width:15%">Type</th>
														<th style="width:15%">Options</th>
														<th style="width:15%">Operations</th>
													</tr>
												</thead>

												<tbody>
													<c:forEach var="question" items="${myquestions}">
														<tr id="tr${question.id}">
															<!-- <td><input type="checkbox" id="qcheckbox${question.id}"/></td> -->
															<td>${question.title}</td>
															<td>${question.description}</td>
															<td>${question.typeS}</td>
															<td><c:forEach var="option"
																	items="${question.options}">
		                                             ${option.optkey}:${option.optvalue} &nbsp;
		                                         </c:forEach></td>
															<td><img onclick="moveUp(${question.id},'tr')"
																src="<%=request.getContextPath()%>/img/up.png"
																class="operationicon"> <img
																onclick="moveDown(${question.id},'tr')"
																src="<%=request.getContextPath()%>/img/down.png"
																class="operationicon"> <img
																onclick="removeQuestionToSurvey(${question.id})"
																src="<%=request.getContextPath()%>/img/delete.png"
																class="operationicon"></td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>

								<div class="usyd-form-component">
									<div class="control-group">
										<label class="control-label" for="textinput"></label>
										<div class="controls">
											<input type="button" id="updateSurvey"
												class="usyd-ui-button-primary" value="Save the change" />
										</div>
									</div>
								</div>

							</fieldset>
						</form>

						<hr />

						<div align="center">
							<form class="form-inline">
								<div class="form-group">
									<input type="text" class="form-control" id="searchContent"
										name="searchContent"
										placeholder="Search interations by student user name"
										style="width:400px" value="${searchContent}">
								</div>
								<input type="button" class="usyd-ui-button-primary"
									value="Search" id="searchButton" />
							</form>
						</div>

						<div id="searchResultDiv" style="">
							<h3>Available question list</h3>
							<table class="table">
								<thead>
									<tr>
										<th style="width:15%">Title</th>
										<th style="width:30%">Description</th>
										<th style="width:15%">Type</th>
										<th style="width:15%">Options</th>
										<th style="width:15%">Operations</th>
									</tr>
								</thead>

								<tbody>
									<c:forEach var="question" items="${questions}">
										<tr>
											<td>${question.title}</td>
											<td>${question.description}</td>
											<td>${question.typeS}</td>
											<td><c:forEach var="option" items="${question.options}">
							         ${option.optkey}:${option.optvalue} &nbsp;
							     </c:forEach></td>
											<td><img onclick="addQuestionToSurvey(${question.id})"
												src="<%=request.getContextPath()%>/img/add.jpg"
												class="operationicon"></td>

										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
