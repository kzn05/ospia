<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>SP Home</title>
<c:import url="/common.jsp"></c:import>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
</head>
<body>
	<c:import url="/spheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div class="jumbotron">
							<h2>${returnContent}</h2>
							<p class="lead">Please review the training and scenario as required before your appointment date.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <c:import url="/footer.jsp"></c:import>

</body>
</html>
