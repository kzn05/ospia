<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>SP Scenario DB</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/sp/11_SP_ScenarioDB.js"></script>
</head>

<body class="usyd-js-load">
	<div class="usyd-page usyd-no-feedback usyd-no-yammer ">
		<c:import url="/spheader.jsp"></c:import>

		<div style="float:right;font-size:14px">
			<a href="">My allocated scenarios</a>
		</div>
		<form class="form-horizontal">
			<fieldset>

				<div class="form-group">
					<label class="col-md-4 control-label" for="textinput"></label>
					<div class="col-md-4">
						<h3>Search Scenarios</h3>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="selectbasic">Age</label>
					<div class="col-md-4 input-group" style="padding-left:15px">
						<select id="age" name="age" class="form-control"
							style="width:100px;">
							<option value="0" selected="selected">All</option>
							<option value="20">20</option>
							<option value="30">30</option>
							<option value="40">40</option>
							<option value="50">50</option>
							<option value="60">60</option>
						</select>
					</div>
				</div>


				<div class="form-group">
					<label class="col-md-4 control-label" for="radios">Gender</label>
					<div class="col-md-4 input-group" style="padding-left:15px">
						<select id="gender" name="gender" class="form-control"
							style="width:100px;">
							<option value="0" selected="selected">All</option>
							<option value="female">Female</option>
							<option value="male">Male</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="selectbasic">Physicality</label>
					<div class="col-md-4 input-group" style="padding-left:15px">
						<select id="physocality" name="physocality" class="form-control"
							style="width:100px;">
							<option value="0" selected="selected">All</option>
							<option value="type1">type1</option>
							<option value="type2">type2</option>
							<option value="type3">type3</option>
							<option value="type4">type4</option>
						</select>
					</div>
				</div>

				<!-- Button -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="singlebutton"></label>
					<div class="col-md-4">
						<input type="button" id="searchSce" name="searchSce"
							class="usyd-ui-button-primary" value="Search">
					</div>
				</div>

			</fieldset>
		</form>

		<h3>Search Result</h3>
		<table class="table">
			<thead>
				<tr>
					<th>Category</th>
					<th>Age</th>
					<th>Gender</th>
					<th>Physocality</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="scenarioList">
			</tbody>
		</table>

		<c:import url="/footer.jsp"></c:import> 

	</div>

</body>
</html>
