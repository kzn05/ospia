<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
    String path = request.getContextPath();
    String basePath =
                    request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                                    + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>

<body>

	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">
						<div class="jumbotron">
							<video id="myvideo" width="640" height="360" controls> <source
								src="<%=request.getContextPath()%>/video/StuTraining.mp4" type="video/mp4"></video>
						</div>

						<div class="jumbotron" style="padding-top:2px;padding-bottom:2px;">
							<h2>OSPIA For Medical Students</h2>
           
<br>            <p class="lead" style=" text-align:justify">
                Finding patients to practice medical communication skills can be tough. 
                The OSPIA has a clever booking system to connect students with simulated patients. 
                Use the calendar system to book and confirm a consultation and practice your communication skills online using video conferencing. 
                What's more, get real time feedback from your simulated patient, use the video capture to review your consultation, and reflect on your communication skills with the addition of non-verbal behavior analysis.
            </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
