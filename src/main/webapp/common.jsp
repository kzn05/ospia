<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<!-- <link rel="icon" href="../../favicon.ico">-->
<link rel="shortcut icon" href="<%=request.getContextPath()%>/img/favicon.ico"
    type="image/vnd.microsoft.icon">


<link type="text/css" rel="stylesheet"
    href="<%=request.getContextPath()%>/css/oldospia/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all">
<link type="text/css" rel="stylesheet"
    href="<%=request.getContextPath()%>/css/oldospia/css_rkmvpe9o6LoWFdKN_dR7nlE_sBBmc9sbbwPtpXa0vwg.css" media="all">
<link type="text/css" rel="stylesheet"
    href="<%=request.getContextPath()%>/css/oldospia/css_YVho6EtfSQcJSXJq-Vrmu5RA7jLxDJjcP_MiQEW_Sgg.css" media="all">
    
<!-- 
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/system.base.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/calendar_multiday.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/date.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/datepicker.1.7.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/field.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/views.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/logintoboggan.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/ckeditor.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/ctools.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/base.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/default.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/compatibility.css" type="text/css">

<style>
.tb-megamenu.animate .mega>.mega-dropdown-menu,.tb-megamenu.animate.slide .mega>.mega-dropdown-menu>div
    {
    transition-delay: 200ms;
    -webkit-transition-delay: 200ms;
    -ms-transition-delay: 200ms;
    -o-transition-delay: 200ms;
    transition-duration: 400ms;
    -webkit-transition-duration: 400ms;
    -ms-transition-duration: 400ms;
    -o-transition-duration: 400ms;
}
</style>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/font-awesome.css" type="text/css">
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/overrides.css">
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia/style.css">
 -->
<!-- 
<link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/justified-nav.css" rel="stylesheet">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/main.min.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/intranet.css" type="text/css">
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/css/bootstrap.min.css" media="all">
 -->
 

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/screen.min.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ospia.css" type="text/css">


<!--[if lt IE 9]>
<script type="text/javascript" src="/etc/designs/intranet/clientlibs/compatibility.min.js"></script>
    <![endif]-->
<!-- 
<link rel="icon" type="image/vnd.microsoft.icon" href="https://intranet.sydney.edu.au/etc/designs/intranet/favicon.ico">
<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="https://intranet.sydney.edu.au/etc/designs/intranet/favicon.ico">
<link rel="apple-touch-icon-precomposed"
	href="https://intranet.sydney.edu.au/etc/designs/intranet/src/lib/uswt-intranet/dist/app/img/apple-touch-icon-precomposed.png">
	 -->
<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o), m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js',
			'ga');

	ga('create', 'UA-66599714-1', 'auto');
	ga('send', 'pageview');
</script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/additional-methods.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/tool.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/moment.js"></script>
</head>
<body>
	<input type="hidden" name="csrftoken" id="csrftoken" value="${csrftoken}">
</body>
</html>
