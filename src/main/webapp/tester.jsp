<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>OSPIA video test</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<!-- <script src='<%=request.getContextPath()%>/js/opentok.min.js'></script> -->
<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
<script src="<%=request.getContextPath()%>/js/tester.js"></script>
<script type="text/javascript">
	var testerApiKey = "${testerApiKey}";
	var testerSessionId = "${testerSession}";
	var testerToken = "${testerToken}";
</script>
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<input id="doctorArchiveIdBaseline" value="" type="hidden">
						<div style="height:700px;">
							<div class="col-lg-8">
								<!-- <h2>Could you please introduce yourself about one minute?</h2> -->
								 <h2>Could you please use several sentences to describe the
                                    weather today?</h2>
                                 <br>
								<div id="baselineContainer"></div>
								<br>
								<input type="button" id="startBaseline" class="usyd-ui-button-primary" value="Start" />
								<input id="testerVideoUrl" type="hidden" value="">
                                <input type="button" id="testerVideoReviewBtn" style="display:none" class="usyd-ui-button-primary" value="Check the video" />
							</div>
							
							<div class="col-lg-3" style="margin-left:15px;">
							     
							    <h2>Instructions</h2>
							    <p>1. Please make sure you are using the Firefox or Chrome web browser.</p>
							    <p>2. Always allow your camera and microphone to be shared.</p>
							    <p>3. When you are ready to record,  click the "Start" button, and answer the question to the left, above the image.  When finished, click the "Stop" button.</p>
                                <p>4. Click the "Check the video" button to review and check the quality of the video recording.</p>    
                                <p>5. If you want to have another try, please refresh this page.</p>
                                <p>6. A good result would be that your head and shoulders are in the shot and your voice is clearly audible. If this is not the case, please check your camera, microphone (including your playback volume setting) and, if necessary, your internet connection in order to improve the set up. Then try again (by refreshing the page). If you continue to have difficulties, please contact csadmin@unsw.edu.au or 9385 2550, in working hours.</p>
							</div>
							
						</div>
					</div>
					
					<div id="waitingPopup" style="display:none">
                            <h4>We are generating your video. Please wait a moment.</h4>
                    </div>
                    
                    <div id="firefoxWarning" style="display:none">
                    <h4>OSPIA cannot run on iPads and only supports Firefox and Chrome web browser sorry for any inconvenience. If you do not have Firefox or Chrome installed, please download them on these pages:</h4>
                        <a href="https://www.mozilla.org/en-US/firefox/new/"  style="font-size:20px;">Download Firefox</a>
                        &nbsp;&nbsp;&nbsp;<a href="https://www.google.com.au/intl/en/chrome/browser/desktop/index.html"  style="font-size:20px;">Download Chrome</a>
                    </div>
                    
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
