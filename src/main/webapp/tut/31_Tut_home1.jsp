<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Tutor Home</title>
<c:import url="/common.jsp"></c:import>
</head>

<body>
	<c:import url="/tutheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b">

						<div id="searchResultDiv" style="">
							<h3>The consultations you need to review</h3>
							<table class="table">
								<thead>
									<tr>
										<th>Date</th>
										<th>Time</th>
										<th>Scenario</th>
										<th>SP Name</th>
										<th>Student Name</th>
										<th>Operations</th>
									</tr>
								</thead>
								<tbody>

									<tr>
										<td>THU 2015-07-16</td>
										<td>13:00</td>
										<td>#12</td>
										<td>Tom Gid</td>
										<td>Chunfeng Liu</td>
										<td><a target="_blank"
											href="reviewscreen_tut/a96fe141-8d66-49bf-a87b-15de05f62d72">
												<button class="usyd-ui-button-primary">Review</button>
										</a></td>
									</tr>

								</tbody>
							</table>
						</div>

						<br> <br>

						<div id="searchResultDiv" style="">
							<h3>Your reviewed consultations in last two weeks</h3>
							<table class="table">
								<thead>
									<tr>
										<th>Date</th>
										<th>Time</th>
										<th>Scenario</th>
										<th>SP Name</th>
										<th>Student Name</th>
										<th>Operations</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>MON 2015-07-15</td>
										<td>09:30</td>
										<td>#13</td>
										<td>Tom Wang</td>
										<td>Chunfeng Liu</td>
										<td><a target="_blank"
											href="vdetail/video_1436761313842&da2812b8-f1f7-4827-91ee-27b7c49cf5db"><button
													class="usyd-ui-button-primary">Reflection</button></a></td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<c:import url="/footer.jsp"></c:import>
</body>
</html>
