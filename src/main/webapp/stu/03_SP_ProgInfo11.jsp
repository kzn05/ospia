<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Program Information</title>
<c:import url="/common.jsp"></c:import>
</head>

<body>
    <c:import url="/stuheader.jsp"></c:import>
    <!-- 
    <div class="main-container container">
		<b>       
		  <a target="_tab" href="<%=request.getContextPath()%>/files/CodeOfConductStu.pdf">
             <img class="pdficon" src="<%=request.getContextPath()%>/img/pdficon.png">
             <span class="pagefont">The Code of Conduct</span>
        </a> &nbsp;&nbsp;&nbsp;&nbsp;
        <a target="_tab" href="<%=request.getContextPath()%>/files/GuideToInterviewStu.pdf">
              <img class="pdficon" src="<%=request.getContextPath()%>/img/pdficon.png">
              <span class="pagefont">Guide to Interview</span>
        </a> &nbsp;&nbsp;&nbsp;&nbsp;
        <a target="_tab" href="<%=request.getContextPath()%>/files/ResearchConsentForm.pdf">
             <img class="pdficon" src="<%=request.getContextPath()%>/img/pdficon.png">
             <span class="pagefont">Research Consent Form</span>
        </a>
		</b>
		
		<br/><br/>
	</div>
	 -->
		<c:import url="/programinfo_stu.jsp"></c:import>
		<c:import url="/footer.jsp"></c:import>
</body>
</html>
