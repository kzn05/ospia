<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student OSPIA Feedback</title>
<c:import url="/common.jsp"></c:import>
<link href="<%=request.getContextPath()%>/css/wufoo/form.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/wufoo/theme.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/js/survey/preview.js"></script>
</head>

<body>
    <c:import url="/stuheader.jsp"></c:import>
    <div id="bodyWrapper">
        <div id="wrapper-second">
            <div class="inner">
                <div id="main-container-b-wrapper">
                    <div id="main-container-b">
                        <form id="Survey_SOCA" name="Survey_SOCA" action="spSOCA_submit">

                            <input type="hidden" id="sessionId" value="${nonverbalSessionId}"> 
                            <input type="hidden" id="doctorId" value="<sec:authentication property="principal.doctor.doctorId"/>"> 
                            <input type="hidden" value="${surveyId}" id="surveyId"> 
                            <input type="hidden" value="${ispreview}" id="ispreview">
                            <input type="hidden" id="nextpage" value="${nextpage}">

                            <header class="info">
                                <h1 id="surveyTitle"></h1>
                                <div>
                                    <p id="surveyDescription"></p>
                                </div>
                            </header>

                            <ul id="questions">
                            </ul>

                        </form>
                    </div>
                </div>
                
                <div id="waitingPopup" style="display:none">
                        <h4>Please wait a moment.</h4>
                    </div>
            </div>
        </div>
    </div>
    <c:import url="/footer.jsp"></c:import>
</body>
</html>
