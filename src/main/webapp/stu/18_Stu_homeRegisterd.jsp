<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="../../favicon.ico">
<title>Student Home</title>
<c:import url="/common.jsp"></c:import>
<script src="<%=request.getContextPath()%>/js/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/js/stu/18_Stu_homeRegisterd.js"></script>
</head>

<body>
	<c:import url="/stuheader.jsp"></c:import>
	<div id="bodyWrapper">
		<div id="wrapper-second">
			<div class="inner">
				<div id="main-container-b-wrapper">
					<div id="main-container-b" style="margin-bottom:200px;">

						<b> <span style="font-size:14px;">Student Name: <span id="stu_name"><sec:authentication
										property="principal.doctor.firstname" /> </span></span><br /> 
						</b>
						<br>

						<h3>Your requests</h3>
						<div id="requestDivLoading" style="display:none">loading... please wait</div>
						<p id="requestNote"></p>
						<div style="height: 200px; overflow-y:auto;">
						<table class="table" id="reqTable" style="display:none">
							<thead>
								<tr>
									<th>Appointment Date</th>
									<th>Appointment Time</th>
									<th>SP Name</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody id="reqList">
							</tbody>
						</table>
                        </div>
                        <br/>
                        
						<h3>Your appointments in the next two weeks</h3>
						<div id="nextaptDivLoading" class="scrollTable" style="display:none">loading... please wait</div>
						<p id="aptNote"></p>
						<div style="height: 200px; overflow-y:auto;">
						<table class="table" id="aptTable" style="display:none">
							<thead>
								<tr>
									<th>Appointment Date</th>
									<th>Appointment Time</th>
									<th>SP Name</th>
									<th></th>
								</tr>
							</thead>
							<tbody id="aptList">
							</tbody>
						</table>
						</div>
						<br/>

						<div id="codeConductPopUp"  style="display:none;height: 560px; overflow: scroll;">
							<table class="table borderless " style="border:0;">
								<tr>
									<td colspan="2"><b>Code of conduct</b>
								</tr>
								<tr>
									<td colspan="2" style="text-align: left;"><b>1. Behaviour and Appearance</b></td>
								</tr>

								<tr>
									<td colspan="2" style="text-align: left;">It is our expectation that your behavior during an OSPIA session will be
										polite, respectful, appropriate and professional in nature. Your use of language should align with these principles -
										swearing is considered inappropriate, and colloquialisms and terminology should be used as appropriate to the SP within
										this professional context. Both parties should be clearly identified at the start of the interaction. Please then ensure
										you address the interviewer/interviewee by the name they have requested to be called. The appearance and dress of
										participants in online interactions should also be appropriate and, for students, reasonably professional.</td>
								</tr>

								<tr>
									<td colspan="2" style="text-align: left;"><b>2. Confidentiality</b></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: left;">Whilst the information in the interview is mostly fictional, some may have
										come from the interviewer/interviewee's own life experience; thus you should treat the OSPIA like a proper interview and
										maintain the confidentiality of the interview information. Further, as this information is part of the teaching
										curriculum, it must not be shared with other students who have not participated with this SP since such sharing will
										decrease their learning experience.</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: left;"><b>3. Failure to abide by the code of conduct</b></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: left;">If behavior is found to be inappropriate during an interaction, the
										interview can be terminated by either party. As all video interactions are recorded, the video will be reviewed and
										appropriate remediation or disciplinary action will take place.</td>
								</tr>
								<tr>
									<td colspan="2"><input type="checkbox" value="1" id="codeCondect">&nbsp; I agree</td>
								</tr>
								<tr>
									<td colspan="2"><input disabled type="button" id="submitCodeConduct" class="usyd-ui-button-primary" value="submit"></td>
								</tr>
							</table>
						</div>
						
						<div class="jumbotron" style="padding-top:100px;padding-bottom:2px;">
                        <hr/>
                            <div>
                                 <div style="text-align:left; margin-top:30px;">
                                 <h5>OSPIA Project Acknowledgements</h5>
                                 <br>
                                 Through the hard work, collaboration and skills of the following people, the OSPIA Project has been made possible. I thank them sincerely for their contributions.
                                 <br/>
                                 <ul style="margin-left:20px;">
                                    <li>Dr Renee Lim, Educational Designer and Director of Program Development, Pam McLean Centre and Positive Computing Lab, Sydney University</li>
                                    <li>Prof Rafael Calvo, Positive Computing Lab at the University of Sydney </li>
                                    <li>Mr Chunfeng Liu, PhD student, Positive Computing Lab at the University of Sydney</li>
                                    <li>Ms Kiran Thwaites, Clinical Skills Administrator, UNSW</li>
                                 </ul>
                                 
                                 </div>
                                 <div style="text-align:right; font-style:italic;">
                                 Dr Silas Taylor, Convenor of Clinical Skills, UNSW Medicine
                                 </div>
                                 <br/>
                                 <div>
                                 <img style="width:350px;height:100px;" src="<%=request.getContextPath()%>/img/gvn_logo.png"><br>
                                 This project received funding from the Australian Government.
                                 </div>
                            </div>
                        </div>
						

						<div id="agreementPopUp" style="display:none;height: 560px; overflow-y: scroll;">
							<input type="hidden" id="doctorId" value="<sec:authentication property="principal.doctor.doctorId"/>" /> 
							<input type="hidden" id="agreement" value="<sec:authentication property="principal.doctor.agreement"/>" /> 
							<input type="hidden" id="homeloaded" value="${homeloaded}" />

							<table class="table borderless " style="border:0;">
								<tr>
                                    <td id="top" colspan="2" style="font-size:18px;"><b>PARTICIPANT INFORMATION STATEMENT AND CONSENT FORM</b>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;"><b>The study is being carried out by the following researchers</b> <br>    
                                    <b>Chief Investigator:</b> <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Dr Silas Taylor (UNSW) <br/>
                                    <b>Co-Investigator/s:</b> <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Prof Rafael Calvo (University of Sydney)<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Dr Renee Lim (University of Sydney)<br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp; A/Prof Boaz Shulruf (UNSW)<br/>
                                    <b>Student Investigator/s:</b> <br>
                                    &nbsp;&nbsp;&nbsp;&nbsp; Chunfeng Liu (PhD student, University of Sydney)
                                    <br/>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;"><b>What is the research study about?</b> <br> 
                                    &nbsp;&nbsp;You are invited to take part in this research study. You have been invited because you are currently enrolled in Phase 1 of the UNSW Medical Program, in which Clinical Skills learning and assessment feature.<br><br>
                                    To participate in this project you need to meet the following inclusion criteria:<br>
                                      &nbsp;&nbsp;&nbsp;&bull; You are currently enrolled in Phase 1 of the UNSW Medical Program, in which Clinical Skills learning and assessment feature.<br>
                                      &nbsp;&nbsp;&nbsp;&bull; You are completing an OSPIA, that is an online interaction with a simulated patient for practice or assessment.<br><br>
                                    &nbsp;&nbsp;The purpose of this research is to provide evidence that medical students will improve their communication skills by using the OSPIA system.
									<br>&nbsp;&nbsp;The first aim of this research project is to help medical students to improve their overall communication skill learning by providing focused and specific feedback on nonverbal skills in online sessions.
									<br>&nbsp;&nbsp;The second aim is more technical: to improve the algorithms that generate the automated feedback. This study will provide further data that will continue to improve the accuracy of computing algorithms that analyse the video and detect nonverbal features of the interactions.
									<br>&nbsp;&nbsp;The third aim is to explore students' engagement with feedback on non-verbal communication behaviours.
									<br>&nbsp;&nbsp;The fourth aim of this project is to investigate the importance of students' autonomy support to simulated patients in the interactions. Autonomy support describes a person in an authority role (e.g., a health care provider) taking into consideration the other's (e.g., the patient's) perspective, acknowledging the other's feelings and perceptions, providing the other with information and choice, and minimizing the use of pressure and control.  Recent evidence shows that providers' behaving in this manner have positive effects on patients' health-promoting behaviors and health status e.g. greater attendance, compliance, adherence and successful behaviour change. In addition, we will measure the correlation between the level of autonomy support and non-verbal communication.
									<br>&nbsp;&nbsp;Importance of the study - Clinical interviews play an important role in the therapeutic process. Medical education thus includes the teaching of communication skills. However, much communication between individuals occurs nonverbally, and this can be difficult to teach students about during live interactions. Providing a sustainable system that aims to improve medical students'� nonverbal communication skills, including provision of good quality automated feedback, is a significant intervention.
                                    <br>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>Do I have to take part in this research study?</b> <br/>
                                    Participation in this research study is voluntary. If you don't wish to take part, you don't have to. Your decision will not affect your relationship with The University of New South Wales or the University of Sydney.
                                    <br><br>
                                    This Participant Information Statement and Consent Form tells you about the research study. It explains the research tasks involved. Knowing what is involved will help you decide if you want to take part in the research.
                                    <br><br>
                                    Please read this information carefully. Ask questions about anything that you don't understand or want to know more about.  Before deciding whether or not to take part, you might want to talk about it with a relative or friend.
                                    <br><br>
                                    If you decide you want to take part in the research study, you will be asked to:
                                    <br>
                                    &nbsp;&nbsp;&nbsp;&bull; Tick 'Agree' on the online consent form;
                                    <br>
                                    &nbsp;&nbsp;&nbsp;&bull; Keep a copy of this Participant Information Statement (available on the OSPIA website)
                                    <br>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What does participation in this research require, and are there any risks involved?</b> <br/>
                                    If you decide to take part in the research study, you will be asked to participate in an OSPIA session, as part of the normal requirements of the UNSW Medicine Clinical Skills curriculum.
                                    <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>Study tasks & risks:</b> <br/>
                                    You will perform an OSPIA interaction, as part of the UNSW Medicine Clinical Skills curriculum. As part of the study, you will need to complete additional tasks:
                                    <br><br>
                                    <u><b>#1</b> Complete a pre-appointment questionnaire:</u> Before you begin the formal OSPIA activity we need to ask you to complete an online pre-appointment questionnaire about your communication skills and attitues to and beliefs about non-verbal communication. We expect this activity to take less than 5 minutes.
                                    <br><br>
                                    <u><b>#2</b> Complete post-appointment questionnaire 1 and autonomy support self-assessment questionnaire:</u> After the formal OSPIA activity we need to ask you to complete an immediate post-appointment questionnaire and the autonomy support self-assessment questionnaire. Straight after the OSPIA activity, you will be asked to complete these online questionnaires about your communication skills and your estimations of your own non-verbal communication mannerisms and your autonomy support to the simulated patient. We expect this activity to take less than 10 minutes. 
									<br><br>
									<u><b>#3</b> Complete a review of your nonverbal behaviour feedback:</u> 48 hours after the OSPIA activity, we will ask you to review your nonverbal feedback, which will require you to log back in to the OSPIA system and view your recording with the accompanying automated feedback. We expect this task to take up to 30 minutes. We will send you email reminders to complete this task.
									<br><br>
									<u><b>#4</b> Complete post-appointment questionnaire 2 and Student learning questionnaire:</u> You will then be asked to complete two further online questionnaire about your non-verbal communication, its importance in interactions, user experience questions, and your engagement with the non-verbal communication feedback. We expect this activity to take up to 20 minutes.
									<br><br> 
									
									&nbsp;&nbsp;&nbsp;#Visit 1 (online, 15 mins): 1) Fill out pre-appointment questionnaire; 2) Complete post-appointment questionnaire 1 and autonomy support self-assessment questionnaire
                                     <br>
                                     &nbsp;&nbsp;&nbsp;#Visit 2 (online, 50 mins): 3) Review nonverbal behaviour feedback ; 4) Complete post-appointment questionnaire 2 and Student learning questionnaire
                                     <br><br>
									
									With your permission we would like to gain access to all assessment and user created content from the OSPIA interaction, namely: all questionnaire data; SOCA assessments; comments relating to your performance entered during an interaction; log files of you interacting with OSPIA system; non-verbal communication behaviour computer analysis files; and identifiable video file recordings of you interacting with the SP. This is in order to assess the students' nonverbal behaviour, compare it to the algorithmic analysis and assessment of the behaviour, and assess the impact of the OSPIA on non-verbal communication behaviour in students in subsequent interactions. The collected information may be reviewed and annotated by authorised experts to assess overall communication skills, and nonverbal communication skills as a subset, in student-SP interactions.
									<br><br> 
									You also have the option to provide consent to your video recordings being shared with other researchers outside of this project for academic purposes only. Researchers use videos to train machine learning algorithms. We would like the videos in which you appear to be a part of a collection they can use for this purpose and this purpose only. Researchers using the videos in which you appear will sign a formal agreement form preventing them from distributing the videos. The researchers will not have access to any data that identifies you (except the images of your face that cannot be anonymised).
									<br><br>
									                                
                                    Aside from giving up your time, we do not expect that there will be any risks or costs associated with taking part in this study.
                                    <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>Will I be paid to participate in this project?</b> <br/>
                                    There are no costs associated with participating in this research study, nor will you be paid.
                                     <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What are the possible benefits to participation?</b> <br/>
                                    We hypothesise that you will benefit from using the system, in terms of increased learning about nonverbal communication skills. We also hope to use information we get from this research study to benefit others who will participate in communication skills teaching in the future.
                                     <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What will happen to information about me?</b> <br/>
                                    By signing the consent form you consent to the research team collecting and using information about you for the research study. We will keep your data for 7 years. We will store information about you at UNSW Kensington and University of Sydney, on password protected staff computers. Your information will only be used for the purpose of this research study and it will only be disclosed with your permission.
                                    <br><br>
                                    It is anticipated that the results of this research study will be published and/or presented in a variety of forums. In any publication and/or presentation, information will be published, in a way such that you will not be individually identifiable.
                                    <br><br>
                                    After each OSPIA we will store the digital video recordings of the interaction for up to one year. We store these files on the UNSW server. Your confidentiality will be ensured by this being a password protected, enterprise quality system. The server can only be accessed by the researchers in the team. You can also access your personal recordings for learning purposes.
                                    <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>How and when will I find out what the results of the research study are?</b> <br/>
                                    You have a right to receive feedback about the overall results of this study. You can tell us that you wish to receive feedback by emailing <a>csadmin@unsw.edu.au</a>. This feedback will be in the form of a link to a webpage that will have a summary of results. You can receive this feedback after the study is completed.
                                    <br><br>
                                    When participating in this study, in addition to the SP feedback and assessment as a normal part of teaching, using this system, you will also receive personalised feedback in the form of computer-generated analysis of your non-verbal communication behaviours during the OSPIA interaction. You are encouraged to access this feedback, available to you 48 hours after the OSPIA interaction.
                                    <br>
                                    </td>
                                </tr>
                                
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What if I want to withdraw from the research study?</b> <br/>
                                    If you do consent to participate, you may withdraw at any time. If you do withdraw, you will be asked to complete and sign the 'Withdrawal of Consent Form' which is provided at the end of this document, and/or as another tick box option on the OSPIA website. Alternatively you can ring the research team and tell them you no longer want to participate.
                                    <br><br>
                                    You are free to stop the OSPIA interaction at any time if it does not comply with the code of conduct - please use the on-screen STOP button. For teaching purposes, these recordings will be reviewed by Dr Silas Taylor. If deemed to not be an appropriate student-SP interview, the research study will delete all analysis of that recording for research purposes. 
                                    <br>Submitting your completed questionnaires is an indication of your consent to participate in the study. You can withdraw your responses if you change your mind about having them included in the study, up to the point that we have analysed the results.
                                    <br>If you decide to withdraw from the study, we will not collect any more information from you. Any non-identifiable information that we have already collected, however, will be kept in our study records and may be included in the study results.
                                    <br>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What should I do if I have further questions about my involvement in the research study?</b> <br/>
                                    The person you may need to contact will depend on the nature of your query. If you want any further information concerning this project or if you have any problems which may be related to your involvement in the project, you can contact the following member/s of the research team:
                                    <br><br>
                                    <b>Research Team Contact</b><br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Name: </b> Dr Silas Taylor <br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Position: </b> Chief Investigator<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Telephone: </b> 9385 2607<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Email: </b> <a>silas.taylor@unsw.edu.au</a><br>
                                    
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b>What if I have a complaint or any concerns about the research study?</b> <br/>
                                    If you have any complaints about any aspect of the project, the way it is being conducted, then you may contact:
                                    <br><br>
                                    <b>Complaints Contact</b><br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Position: </b> Human Research Ethics Coordinator<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Telephone: </b> + 61 2 9385 6222<br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>Email: </b> <a>humanethics@unsw.edu.au</a><br>
                                    &nbsp;&nbsp;&nbsp;&bull; <b>HC Reference Number: </b> HC16048 <br>
                                    </td>
                                </tr>
                                
                                 <tr>
                                    <td colspan="2" style="text-align: left;">
                                    <b style="color:red;font-size:18px;">I would like to participate and I have</b> <br/>
                                    &nbsp;&nbsp;&nbsp;&bull; Read the Participant Information Sheet or someone has read it to me in a language that I understand;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Understood the purposes, study tasks and risks of the research described in the project;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Had an opportunity to email questions to the Chief Investigator and I am satisfied with the answers I have received;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Freely agree to participate in this research study as described and understand that I am free to withdraw at any time during the project and withdrawal will not affect my relationship with any of the named organisations and/or research team members;<br><br>
                                    &nbsp;&nbsp;&nbsp;&bull; Understand that I am able to down load a copy of this document to keep from the OSPIA website (student login)<br><br>
                                    </td>
                                </tr>
                                
								<tr>
									<td colspan="2">Do you agree above items?</td>
								</tr>
								<tr>
									<td style="width:50%"><input type="radio" value="1" name="agreement" checked="checked">&nbsp;I agree</td>
									<td style="width:50%"><input type="radio" value="0" name="agreement">&nbsp;I don't agree</td>
								</tr>
								<tr>
									<td colspan="2"><input type="button" id="submitAgreement" class="usyd-ui-button-primary" value="submit"></td>
								</tr>
							</table>
						</div>
						
						<div id="waitingPopup" style="display:none">
	                        <h4>OSPIA only supports Firefox and Chrome web browsers, sorry for any inconvenience. If you do not have Firefox installed, please download it on this page:</h4>
	                        <a href="https://www.mozilla.org/en-US/firefox/new/"  style="font-size:20px;">https://www.mozilla.org/en-US/firefox/new/</a>
	                    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:import url="/footer.jsp"></c:import>
</body>
</html>
